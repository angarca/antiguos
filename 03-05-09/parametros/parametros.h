#ifndef parametros_h
#define parametros_h

#include <stdio.h>

namespace EA{

	void parametros(const char *constante,int &valor,const char entrada[]="parametros.txt"){
		char encontrada=0;
		FILE *archivo=fopen(entrada,"rb");
		for(int i=0;constante[i]!='\0';i++);
		char buf[++i];
		do{
			for(i=0;(!feof(archivo))&&((buf[i]=fgetc(archivo))!=' ');i++);buf[i]='\0';
			if(strcmp(constante,buf)){
				encontrada=1;
				valor=fgetc(archivo);
			}
		}while((!feof(archivo))&&(!encontrada));
		fclose(archivo);
		if(!encontrada){
			archivo=fopen("parametros_debug.txt","a");
			for(i=0;constante[i]!='\0';i++)fputc(constante[i],archivo);
			fputc(10,archivo);
			fclose(archivo);
		}
	}

}

#endif//parametros_h