#include <stdio.h>
#include <SDL.h>

int main(int argc,char *argv[]){
	int done=1,i,j;
	char cad[20];
	SDL_Surface *screen,*capas[10],*blanco;
	SDL_Rect reccas,posc1,posc2,posc3,dest;
	SDL_Event event;
	struct cursor{
		int pos;
		SDL_Surface *img;
	}Cursor;
	//inicializar
	Cursor.pos=1;
	//valores de rectangulos
	//  para truncar casillas
	reccas.x=0;
	reccas.y=0;
	reccas.w=64;
	reccas.h=64;
	//  para posicion de capa1
	posc1.x=0;
	posc1.y=0;
	//  para posicion de capa2
	posc2.x=0;
	posc2.y=64*2;
	//  para posicion de capa3
	posc3.x=0;
	posc3.y=64*4;
	//fin
	//inicializa SDL
	SDL_Init(SDL_INIT_VIDEO);
	screen=SDL_SetVideoMode(64*9,64*5,24,SDL_HWSURFACE);
	//fin
	//carga imagen en blanco
	blanco=SDL_LoadBMP("blanco.bmp");
	//fin
	//carga imagenes de capas
	for(i=0;i<9;i++){
		sprintf(cad,"capa%d.bmp",i+1);
		capas[i]=SDL_LoadBMP(cad);
		SDL_SetColorKey(capas[i],SDL_SRCCOLORKEY|SDL_RLEACCEL,SDL_MapRGB(capas[i]->format,255,255,255));
	}
	//fin
	//carga cursor
	Cursor.img=SDL_LoadBMP("cursor.bmp");
	SDL_SetColorKey(Cursor.img,SDL_SRCCOLORKEY|SDL_RLEACCEL,SDL_MapRGB(Cursor.img->format,255,255,255));
	//fin
	//loop
	while(done){
		//blanquear la pantalla
		for(i=0;i<9;i++){	
			for(j=0;j<5;j++){
				dest.x=i*64;	
				dest.y=j*64;
				SDL_BlitSurface(blanco,NULL,screen,&dest);
			}
		}
		//fin
		//actualizar pantalla
		if(Cursor.pos>1)SDL_BlitSurface(capas[Cursor.pos-2],&reccas,screen,&posc1);
		else SDL_BlitSurface(blanco,&reccas,screen,&posc1);
		SDL_BlitSurface(capas[Cursor.pos-1],&reccas,screen,&posc2);
		if(Cursor.pos<9)SDL_BlitSurface(capas[Cursor.pos],&reccas,screen,&posc3);
		else SDL_BlitSurface(blanco,&reccas,screen,&posc3);
		SDL_BlitSurface(Cursor.img,&reccas,screen,&posc2);
		//fin
		//mostrar pantalla
		SDL_Flip(screen);
		//fin
		//gestionar entrada
		if(SDL_PollEvent(&event))if(event.type==SDL_KEYDOWN){
			switch(event.key.keysym.sym){
			case SDLK_UP:
				if(Cursor.pos>1)Cursor.pos--;
				break;
			case SDLK_DOWN:
				if(Cursor.pos<9)Cursor.pos++;
				break;
			case SDLK_RIGHT:
				break;
			case SDLK_LEFT:
				break;
			case SDLK_c:
				break;
			case SDLK_s:
				break;
			case SDLK_m:
				break;
			default:
				printf("\nentrada : ");
				scanf("%s",cad);
				if(cad[0]=='c')done=0;
			}
		}
	}
	//fin
	//liberar recursos
	SDL_FreeSurface(blanco);
	SDL_FreeSurface(Cursor.img);
	for(i=0;i<9;i++){
		SDL_FreeSurface(capas[i]);
	}
	SDL_Quit();
	return 0;
}