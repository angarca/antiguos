#include <SDL.h>
#include "obj_TTF.h"

int main(int argc,char *argv[]){
	SDL_Event evento;
	SDL_Surface *pan,*txt;
	char cad[10]={1,2,3,4,'\0'};

	SDL_Init(SDL_INIT_VIDEO);
	pan=SDL_SetVideoMode(640,480,32,SDL_HWSURFACE);

	obj_SDL::ttf.establecer(10,20,"TTF.bmp");
	txt=obj_SDL::ttf.escrivir(cad);

	SDL_BlitSurface(txt,NULL,pan,NULL);
	SDL_Flip(pan);

	while(!(SDL_PollEvent(&evento)&&(evento.type==SDL_KEYDOWN)));

	SDL_FreeSurface(txt);
	SDL_Quit();
	return 0;

}