#ifndef fun_IMG_h
#define fun_IMG_h

#include <SDL.h>

namespace obj_SDL{

	void PutPixel(SDL_Surface *img,int x,int y,Uint32 pixel){
		if(((x<img->w)&&(y<img->h))&&((x>=0)&&(y>=0))){
			if(!(SDL_LockSurface(img)<0)){

				int bpp=img->format->BytesPerPixel;
				Uint8 *p=(Uint8 *)img->pixels+y*img->pitch+x*bpp;

				switch(bpp){
				case 1:
					*p=(Uint8)pixel;
					break;
				case 2:
					*(Uint16 *)p=(Uint16)pixel;
					break;
				case 3:
					if(SDL_BYTEORDER==SDL_BIG_ENDIAN){
						p[0]=((Uint8)pixel>>16)&0xff;
						p[1]=((Uint8)pixel>>8)&0xff;
						p[2]=(Uint8)pixel&0xff;
					}else{
						p[0]=(Uint8)pixel&0xff;
						p[1]=((Uint8)pixel>>8)&0xff;
						p[2]=((Uint8)pixel>>16)&0xff;
					}
					break;
				case 4:
					*(Uint32 *)p=pixel;
					break;
				}

				SDL_UnlockSurface(img);
			}
		}
	};

	void DrawLine(SDL_Surface *img,int x1,int y1,int x2,int y2,Uint32 pixel){
		float x,y,p;
		if(x1==x2){
			x=(float)x1;
			if(y1<y2){
				y=(float)y1;
				while(y<=y2)PutPixel(img,(int)x,(int)(y++),pixel);
			}else{
				y=(float)y2;
				while(y<=y1)PutPixel(img,(int)x,(int)(y++),pixel);
			}
		}else{
			if(x1<x2){
				x=(float)x1;
				y=(float)y1;
				p=(float)(y2-y)/(float)(x2-x);
				while(x<x2){
					PutPixel(img,(int)(x++),(int)(y+=p),pixel);
				}
				PutPixel(img,(int)x2,(int)y2,pixel);
			}else{
				x=(float)x2;
				y=(float)y2;
				p=(float)(y1-y)/(float)(x1-x);
				while(x<x1){
					PutPixel(img,(int)(x++),(int)(y+=p),pixel);
				}
				PutPixel(img,(int)x1,(int)y1,pixel);
			}

		}
	};

}

#endif//fun_IMG_h