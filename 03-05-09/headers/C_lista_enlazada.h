#ifndef C_lista_enlazada_h
#define C_lista_enlazada_h

namespace C_lista_enlazada{

	//declaraciones

	enum metodos{COLA,PILA};

	template <class T>
	class nodo{
	private:
		nodo *sig;
		T *obj;
	public:
		nodo(T *t);
		~nodo(void);
		T *operator[](int i);
		T *leer(metodos met);
		T *escrivir(T *OBJ);
		T *insertar(T *OBJ,int i);
	};

	//definiciones

	template <class T>
	nodo<T>::nodo(T *t){
		sig=NULL;
		obj=t;
	}

	template <class T>
	nodo<T>::~nodo(){
		if(sig)delete sig;
		if(obj)delete obj;
	}

	template <class T>
	T *nodo<T>::operator[](int i){
		if(i<0)return NULL;
		if(i&&sig)return (*sig)[--i];
		return obj;
	}

	template <class T>
	T *nodo<T>::leer(metodos met){
		nodo *aux;
		T *AUX;
		if(!obj)return NULL;
		switch(met){
		case COLA:
			AUX=obj;
			if(sig){
				obj=sig->obj;
				sig->obj=NULL;
				aux=sig->sig;
				sig->sig=NULL;
				delete sig;
				sig=aux;
			}
			else obj=NULL;
			return AUX;
		case PILA:
			AUX=obj;
			if(sig){
				aux=this;
				while(aux->sig->sig)aux=aux->sig;
				AUX=aux->sig->obj;
				aux->sig->obj=NULL;
				delete aux->sig;
				aux->sig=NULL;
			}
			else obj=NULL;
			return AUX;
		default:
			return NULL;
		}
	}

	template <class T>
	T *nodo<T>::escrivir(T *OBJ){
		if(sig){
			return sig->escrivir(OBJ);
		}
		else{
			return (sig=new nodo(OBJ))->obj;
		}
	}

	template <class T>
	T *nodo<T>::insertar(T *OBJ,int i){
		if(sig&&(i--)){
			return sig->insertar(OBJ,i);
		}else if(!sig){
			return (sig=new nodo(OBJ))->obj;
		}else{
			nodo *aux=new nodo(OBJ);
			aux->sig=sig;
			sig=aux;
			return sig->obj;
		}
	}

}

#endif//C_lista_enlazada_h