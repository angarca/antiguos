#ifndef obj_TTF_h
#define obj_TTF_h

#include <SDL.h>

namespace obj_SDL{

	namespace obj_TTF{

		//declaraciones
		class TTF{
		private:
			SDL_Rect tam;
			SDL_Surface *img;
		public:
			TTF();
			~TTF();
			void establecer(int W,int H,char *IMG);
			SDL_Surface *escrivir(char *txt);
		};

		//definiciones
		TTF::TTF(){
			img=NULL;
		}

		TTF::~TTF(){
			if(img)SDL_FreeSurface(img);
		}

		void TTF::establecer(int W,int H,char *IMG){
			img=SDL_LoadBMP(IMG);
			tam.w=W;
			tam.h=H;
		}

		SDL_Surface *TTF::escrivir(char *txt){
			SDL_Surface *ret;
			SDL_Rect aux;
			int i;
			aux.x=0;
			aux.y=0;
			tam.y=0;
			for(i=0;txt[i]!='\0';i++);
			ret=SDL_CreateRGBSurface(SDL_SWSURFACE,i*tam.w,tam.h,32,0,0,0,0);
			for(i=0;txt[i]!='\0';i++){
				tam.x=(tam.w)*(txt[i]);
				SDL_BlitSurface(img,&tam,ret,&aux);
				aux.x+=tam.w;
			}
			return ret;
		}

	}

	//declarar objetos particulares
	obj_TTF::TTF ttf;

}

#endif//obj_TTF_h