#include <stdio.h>
#include <SDL.h>
#include "cmapa.h"

//tama�o de pantalla
int An=800,Al=600,AnPa=100;

//variables globales
	int salidas=0x00000000;
	cmapa::mapa *Mapa;
	int faseact=0,fasemax=0;
	int multicapa=0;
	SDL_Rect morigen,mdestino;
//fin

int main(int argc,char *argv[]){

	//variables de SDL
	SDL_Surface *pantalla,*panmap=NULL,*pansprite=NULL,*panblanc;
	SDL_Rect origen,destino;
	SDL_Event evento;
	Sint32 time,ftime;
	//variables de cadena
	char cad[30],cad2[30];
	//variables de entrada
	int AnT,AlT,AnM,AlM;
	int num;
	//variables de posicion
	int Capa;
	struct cursor{
		int x,y;
		int pincel;
		SDL_Surface *img;
		cursor(){
			pincel=0;
			img=SDL_LoadBMP("cursor.bmp");
			SDL_SetColorKey(img,SDL_SRCCOLORKEY|SDL_RLEACCEL,SDL_MapRGB(img->format,255,255,255));
		};
	}Cursor;
	do{
		//entrada de valores generales
		printf("\nquieres abrir o crear un mapa (a/c)? : ");
		scanf("%s",&cad);
		if(cad[0]=='a'){
		//	abrir
			FILE *archivo;
			printf("\nel nombre del archivo por favor : ");
			scanf("%s",&cad);
			Mapa=new cmapa::mapa(cad);
			archivo=fopen(cad,"rb");
			AnM=fgetc(archivo);
			AnT=fgetc(archivo);
			AlM=fgetc(archivo);
			AlT=fgetc(archivo);
			fclose(archivo);
		}
		else {
		//	crear
		printf("\nque ancho de tiles quieres ?(pixeles) : ");
		scanf("%d",&AnT);
		printf("\nque alto de tiles quieres ?(pixeles) : ");
		scanf("%d",&AlT);
		printf("\nque ancho de mapa quieres ?(tiles) : ");
		scanf("%d",&AnM);
		printf("\nque alto de mapa quieres ?(tiles) : ");
		scanf("%d",&AlM);
		Mapa=new cmapa::mapa(AnT,AlT,AnM,AlM);
		}
		//inicalizar variables de posicion
		Capa=0;
		Cursor.x=0;
		Cursor.y=0;
		//iniciar SDL
		SDL_Init(SDL_INIT_VIDEO);
		pantalla=SDL_SetVideoMode(An+AnPa,Al,24,SDL_HWSURFACE|SDL_DOUBLEBUF|SDL_RESIZABLE);
		panblanc=SDL_CreateRGBSurface(SDL_SWSURFACE,An+AnPa,Al,24,0,0,0,0);
		ftime=SDL_GetTicks();
		//loop de control
		do{
			//tiempo a la entrada
			time=SDL_GetTicks();
		//	componer la pantalla
			//	blanquear la pantalla
			SDL_BlitSurface(panblanc,NULL,pantalla,NULL);
			//	fin
			//re/iniciar pantalla inermedia para el mapa
			if(panmap!=NULL)SDL_FreeSurface(panmap);
			panmap=SDL_CreateRGBSurface(SDL_SWSURFACE,An,Al,24,0,0,0,0);
			//componer pantalla intermedia para el mapa
			origen.x=(Cursor.x*AnT)-((An/2)-(AnT/2));
			origen.y=(Cursor.y*AlT)-((Al/2)-(AlT/2));
			origen.w=An;
			origen.h=Al;
			destino.x=0;
			destino.y=0;
			//si multicapa se carga cada capa de fondo
			if(multicapa){
				for(multicapa=0;multicapa<Capa;multicapa++)SDL_BlitSurface((*Mapa)(multicapa),&(morigen=origen),panmap,&(mdestino=destino));
				multicapa=1;
			}
			//se carga la capa actual
			SDL_BlitSurface((*Mapa)(Capa),&origen,panmap,&destino);
			//situar cursor en la pantalla intermedia del mapa
			origen.x=0;
			origen.y=0;
			origen.w=AnT;
			origen.h=AlT;
			destino.x=(An/2)-(AnT/2);
			destino.y=(Al/2)-(AlT/2);
			SDL_BlitSurface(Cursor.img,&origen,panmap,&destino);
			//re/iniciar la pantalla intermadia de sprites
			if(pansprite!=NULL)SDL_FreeSurface(pansprite);
			pansprite=SDL_CreateRGBSurface(SDL_SWSURFACE,AnPa,Al,24,0,0,0,0);
			//componer la pantalla de sprites
			origen.x=0;
			origen.y=(Cursor.pincel*AlT)-((Al/2)-(AlT/2));
			origen.w=AnPa;
			origen.h=Al;
			destino.x=0;
			destino.y=0;
			SDL_BlitSurface(Mapa->leer_paleta(Cursor.pincel),&origen,pansprite,&destino);
			//componer pantalla final
			origen.x=0;
			origen.y=0;
			origen.w=An;
			origen.h=Al;
			destino.x=0;
			destino.y=0;
			SDL_BlitSurface(panmap,&origen,pantalla,&destino);
			origen.x=0;
			origen.y=0;
			origen.w=AnPa;
			origen.h=Al;
			destino.x=An;
			if(AnT<AnPa)destino.x=(An+AnPa)-AnT;
			destino.y=0;
			SDL_BlitSurface(pansprite,&origen,pantalla,&destino);
		//	mostrar la pantalla
			SDL_Flip(pantalla);
		//	gestionar entrada
			if(SDL_PollEvent(&evento)){if(evento.type==SDL_KEYDOWN){
				switch(evento.key.keysym.sym){
				case SDLK_UP:
					salidas&=0xfffffffd;
					if(Cursor.y>0)Cursor.y--;
					break;
				case SDLK_DOWN:
					salidas&=0xfffffffd;
					if(Cursor.y<(AlM-1))Cursor.y++;
					break;
				case SDLK_LEFT:
					salidas&=0xfffffffd;
					if(Cursor.x>0)Cursor.x--;
					break;
				case SDLK_RIGHT:
					salidas&=0xfffffffd;
					if(Cursor.x<(AnM-1))Cursor.x++;
					break;
				case SDLK_SPACE:
					(*Mapa)(Capa,Cursor.x,Cursor.y,Cursor.pincel);
					break;
				case SDLK_TAB:
					//volver al editor de comandos
					SDL_Quit();
					//
					printf("\nutilizar fase propia o general (p/g)? : ");
					scanf("%s",&cad);
					if(cad[0]=='p'){
						printf("\nla fase acual por favor (0-15) : ");
						scanf("%d",&num);
						if((num>=0)&&(num<=15))(*Mapa)(Capa,Cursor.x,Cursor.y,1,num);
					}
					else{
						(*Mapa)(Capa,Cursor.x,Cursor.y,0,0);
					}
					//volver a SDL
					SDL_Init(SDL_INIT_VIDEO);
					pantalla=SDL_SetVideoMode(An+AnPa,Al,24,SDL_HWSURFACE|SDL_DOUBLEBUF|SDL_RESIZABLE);
					break;
				case SDLK_z:
					if(Cursor.pincel>0)Cursor.pincel--;
					break;
				case SDLK_x:
					Cursor.pincel++;
					break;
				case SDLK_c:
					if(multicapa)multicapa=0;
					else multicapa=1;
					break;
				case SDLK_r:
					Mapa->rellenar(Capa,Cursor.x,Cursor.y,Cursor.pincel);
					break;
				default:
					//volver al editor de comandos
					SDL_Quit();
					//
					printf("\nentrada : ");
					scanf("%s",&cad);
					if(!strcmp(cad,"addcapa")){
						printf("\nen que posicion quieres insertar la nueva capa (0-...)|(-) : ");
						scanf("%d",&num);
						Mapa->addcapa(num);
					}
					if(!strcmp(cad,"addsprite")){
						printf("\nel nombre del frame base del sprite por favor : ");
						scanf("%s",&cad2);
						Mapa->addsprite(cad2);
					}
					if(!strcmp(cad,"addframe")){
						printf("\nel sprite al que se deve a�adir (1-...) : ");
						do{
							scanf("%d",&num);
							if(num<=0)printf("\nese numero no es valido por favor introduce uno mayor que cero : ");
						}while(num<=0);
						printf("\nel nombre del frame que se deve a�adir por favor : ");
						scanf("%s",&cad2);
						(*Mapa)(num,cad2);
					}
					if(!strcmp(cad,"cambiar_pincel")){
						printf("\nelige el sprite que se usara como pincel (0=blanco) (1-...) : ");
						scanf("%d",&num);
						if(num>=0)Cursor.pincel=num;
					}
					if(!strcmp(cad,"cambiar_fasemax")){
						printf("\nelige el numero maximo de fases (0-...) : ");
						scanf("%d",&num);
						if(num>=0)fasemax=num;
					}
					if(!strcmp(cad,"cambiar_capa")){
						printf("\nelige la capa que quieres editar (0-...) : ");
						scanf("%d",&num);
						if(num>=0)Capa=num;
					}
					if(!strcmp(cad,"borrar_capa")){
						printf("\nque capa deseas borrar (0-...) : ");
						scanf("%d",&num);
						if(num>=0)Mapa->erasecapa(num);
					}
					if(!strcmp(cad,"borrar_sprite")){
						printf("\nque sprite deseas borrar (0-...) :");
						scanf("%d",&num);
						if(num>=0)Mapa->erasesprite(num);
					}
					if(!strcmp(cad,"borrar_frame")){
						int n;
						printf("\na que sprite quieres quitarle un frame (0-...) : ");
						scanf("%d",&num);
						if(num>=0)n=num;
						printf("\ncual de los frames delsprite quieres borrar (0-...) : ");
						scanf("%d",&num);
						if(num>=0)Mapa->eraseframe(n,num);
					}
					if(!strcmp(cad,"posicion")){
						printf("\nestos son los datos de tu posicion actual :");
						printf("\n\tcapa : %d\tcoordenadas : %d,%d",Capa,Cursor.x,Cursor.y);
					}
					if(!strcmp(cad,"pincel")){
						printf("\nestas usando el pincel N� %d actualmente",Cursor.pincel);
					}
					if(!strcmp(cad,"cerrar"))salidas|=0x00000002;
					//volver a SDL
					else{
						SDL_Init(SDL_INIT_VIDEO);
						pantalla=SDL_SetVideoMode(An+AnPa,Al,24,SDL_HWSURFACE|SDL_DOUBLEBUF|SDL_RESIZABLE);
					}
					//si se cierra el mapa no retorna a SDL
				}
			}else{
				salidas&=0xfffffffd;
				if(evento.type==SDL_VIDEORESIZE){
					Al=evento.resize.h;
					AnPa=(evento.resize.w/10);
					An=(evento.resize.w-AnPa);
					SDL_FreeSurface(panblanc);
					pantalla=SDL_SetVideoMode(An+AnPa,Al,24,SDL_HWSURFACE|SDL_DOUBLEBUF|SDL_RESIZABLE);
					panblanc=SDL_CreateRGBSurface(SDL_SWSURFACE,An+AnPa,Al,24,0,0,0,0);
				}
			}}
			//cambiar fase general acorde al tiempo
			if(faseact>fasemax)faseact=0;
			if((SDL_GetTicks()-ftime)>1000){ftime=SDL_GetTicks();(*Mapa)>>(faseact++);}
			//calcular tiempo sobrante y esperarlo
			if((time=(50-(SDL_GetTicks()-time)))>0)SDL_Delay(time);
		}while(!(salidas&0x00000002));
		//guardar?
		printf("\nquieres guardar el mapa antes de cerrarlo (s/n)? : ");
		scanf("%s",&cad);
		if(cad[0]=='s'){
			printf("\nel nombre del archivo en el que se guardara por favor : ");
			scanf("%s",&cad);
			Mapa->guardar(cad);
		}
		//liberar recursos
		delete Mapa;
		SDL_FreeSurface(panblanc);
		//salir?
		printf("\ndeseas abandonar el programa (s/n)? : ");
		scanf("%s",&cad);
		if(cad[0]=='s')salidas|=0x00000001;
		else salidas&=0xfffffffe;
	//retorno
	}while(!(salidas&0x00000001));
	return 0;

}