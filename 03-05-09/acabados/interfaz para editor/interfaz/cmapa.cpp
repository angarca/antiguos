#include <stdio.h>
#include <SDL.h>
#include "cmapa.h"

namespace cmapa{

	//funciones de la clase mapa

	mapa::mapa(int AnT,int AlT,int AnM,int AlM){
		X=AnT;
		Y=AlT;
		x=AnM;
		y=AlM;
		paleta=new sprite("blanco.bmp");
		capas=new capa(x,y);
		img=SDL_CreateRGBSurface(SDL_SWSURFACE,AnT*AnM,AlT*AlM,24,0,0,0,0);
		imgb=SDL_CreateRGBSurface(SDL_SWSURFACE,AnT*AnM,AlT*AlM,24,0,0,0,0);
		org.x=0;org.y=0;org.w=X;org.h=Y;dest.w=X;dest.h=Y;
		f=0;
		imgs=NULL;
	}

	mapa::~mapa(){
		delete paleta;
		delete capas;
		SDL_FreeSurface(img);
	}

	void mapa::addcapa(int p){
		capa *aux,*aux2;
		aux=capas;
		if(p<0){
			while(aux->sig)aux=aux->sig;
			aux->sig=new capa(x,y);
		}
		else if(p){
			while((aux->sig)&&((--p)>0))aux=aux->sig;
			aux2=aux->sig;
			aux->sig=new capa(x,y);
			aux->sig->sig=aux2;
		}
		else{
			capas=new capa(x,y);
			capas->sig=aux;
		}
	}

	void mapa::addsprite(char *name){
		sprite *aux;
		aux=paleta;
		while(aux->sig)aux=aux->sig;
		aux->sig=new sprite(name);
	}

	void mapa::erasecapa(int c){
		capa *aux,*aux2;
		if(c>=0){
			if(c){
				if(capas->sig){
					aux=capas;
					while((aux->sig->sig)&&((--c)>0))aux=aux->sig;
					aux2=aux->sig;
					aux->sig=aux2->sig;
					aux2->sig=NULL;
					delete aux2;
				}
			}
			else{
				if(capas->sig){
					aux=capas;
					capas=capas->sig;
					aux->sig=NULL;
					delete aux;
				}
			}
		}
	}

	void mapa::erasesprite(int s){
		sprite *aux,*aux2;
		if(s>=0){
			if(s){
				if(paleta->sig){
					aux=paleta;
					while((aux->sig->sig)&&((--s)>0))aux=aux->sig;
					aux2=aux->sig;
					aux->sig=aux2->sig;
					aux2->sig=NULL;
					delete aux2;
				}
			}
			else{
				if(paleta->sig){
					aux=paleta;
					paleta=paleta->sig;
					aux->sig=NULL;
					delete aux;
				}
			}
		}
	}

	void mapa::eraseframe(int s,int F){
		sprite *auxf=paleta;
		frame *aux,*aux2;
		if(s>=0){
			while((auxf->sig)&&((s--)>0))auxf=auxf->sig;
			if(F>=0){
				if(F){
					if(auxf->frames->sig){
						aux=auxf->frames;
						while((aux->sig->sig)&&((--F)>0))aux=aux->sig;
						aux2=aux->sig;
						aux->sig=aux2->sig;
						aux2->sig=NULL;
						delete aux2;
					}
				}
				else{
					if(auxf->frames->sig){
						aux=auxf->frames;
						auxf->frames=auxf->frames->sig;
						aux->sig=NULL;
						delete aux;
					}
				}
			}
		}
	}

	void mapa::rellenar(int c,int x,int y,int s){
		int i,j,co;
		capa *aux;
		if(c>=0){
			aux=capas;
			while(((c--)>0)&&aux->sig)aux=aux->sig;
			co=aux->tabla[x][y].flag&0x000003ff;
			aux->tabla[x][y].flag&=0xfffffc00;
			aux->tabla[x][y].flag|=(s&0x000003ff);
			for(i=x+1;(i<(this->x))&&((aux->tabla[i][y].flag)==co);i++){
				aux->tabla[i][y].flag&=0xfffffc00;
				aux->tabla[i][y].flag|=(s&0x000003ff);
				for(j=y+1;(j<(this->y))&&((aux->tabla[i][j].flag)==co);j++){
					aux->tabla[i][j].flag&=0xfffffc00;
					aux->tabla[i][j].flag|=(s&0x000003ff);
				}
				for(j=y-1;(j>=0)&&((aux->tabla[i][j].flag)==co);j--){
					aux->tabla[i][j].flag&=0xfffffc00;
					aux->tabla[i][j].flag|=(s&0x000003ff);
				}
			}
			for(i=x-1;(i>=0)&&((aux->tabla[i][y].flag)==co);i--){
				aux->tabla[i][y].flag&=0xfffffc00;
				aux->tabla[i][y].flag|=(s&0x000003ff);
				for(j=y+1;(j<(this->y))&&((aux->tabla[i][j].flag)==co);j++){
					aux->tabla[i][j].flag&=0xfffffc00;
					aux->tabla[i][j].flag|=(s&0x000003ff);
				}
				for(j=y-1;(j>=0)&&((aux->tabla[i][j].flag)==co);j--){
					aux->tabla[i][j].flag&=0xfffffc00;
					aux->tabla[i][j].flag|=(s&0x000003ff);
				}
			}
			for(i=y+1;(i<(this->y))&&((aux->tabla[x][i].flag)==co);i++){
				aux->tabla[x][i].flag&=0xfffffc00;
				aux->tabla[x][i].flag|=(s&0x000003ff);
				for(j=x+1;(j<(this->x))&&((aux->tabla[j][i].flag)==co);j++){
					aux->tabla[j][i].flag&=0xfffffc00;
					aux->tabla[j][i].flag|=(s&0x000003ff);
				}
				for(j=x-1;(j>=0)&&((aux->tabla[j][i].flag)==co);j--){
					aux->tabla[j][i].flag&=0xfffffc00;
					aux->tabla[j][i].flag|=(s&0x000003ff);
				}
			}
			for(i=y-1;(i>=0)&&((aux->tabla[x][i].flag)==co);i--){
				aux->tabla[x][i].flag&=0xfffffc00;
				aux->tabla[x][i].flag|=(s&0x000003ff);
				for(j=y+1;(j<(this->x))&&((aux->tabla[i][j].flag)==co);j++){
					aux->tabla[j][i].flag&=0xfffffc00;
					aux->tabla[j][i].flag|=(s&0x000003ff);
				}
				for(j=y-1;(j>=0)&&((aux->tabla[i][j].flag)==co);j--){
					aux->tabla[j][i].flag&=0xfffffc00;
					aux->tabla[j][i].flag|=(s&0x000003ff);
				}
			}
		}
	}

	void mapa::operator()(int s,char *name){
		sprite *aux;
		frame *aux2;
		aux=paleta;
		if(s>0){
			while(((s--)>0)&&aux->sig)aux=aux->sig;
			aux2=aux->frames;
			while(aux2->sig)aux2=aux2->sig;
			aux2->sig=new frame(name);
		}
	}

	void mapa::operator()(int c,int x,int y,int val){
		capa *aux;
		if(c>=0){
			aux=capas;
			while(((c--)>0)&&aux->sig)aux=aux->sig;
			aux->tabla[x][y].flag&=0xfffffc00;
			aux->tabla[x][y].flag|=(val&0x000003ff);
		}
	}

	void mapa::operator()(int c,int x,int y,int F,int fase){
		capa *aux;
		if(c>=0){
			aux=capas;
			while(((c--)>0)&&aux->sig)aux=aux->sig;
		}
		if(F){
			aux->tabla[x][y].flag|=0x80000000;
			aux->tabla[x][y].flag&=0xfffc03ff;
			aux->tabla[x][y].flag|=((fase*1024)&0x0003fc00);
		}
		else{
			aux->tabla[x][y].flag&=0x7fffffff;
		}
	}

	SDL_Surface *mapa::operator()(int c){
		int i,j,F,t;
		SDL_Surface *aux;
		capa *aux2;
		sprite *aux3;
		frame *aux4;
		SDL_BlitSurface(imgb,NULL,img,NULL);
		aux2=capas;
		while((aux2->sig)&&((c--)>0))aux2=aux2->sig;
		for(i=0;i<x;i++)for(j=0;j<y;j++){
			aux3=paleta;
			dest.x=i*X;
			dest.y=j*Y;
			if(aux2->tabla[i][j].flag&0x80000000){
				F=(aux2->tabla[i][j].flag&0x0003fc00)/1024;
			}
			else F=f;
			t=aux2->tabla[i][j].flag&0x000003ff;
			while((aux3->sig)&&((t--)>0))aux3=aux3->sig;
			aux4=aux3->frames;
			while((aux4->sig)&&((F--)>0))aux4=aux4->sig;
			aux=aux4->img;
			SDL_BlitSurface(aux,&org,img,&dest);
		}
		SDL_SetColorKey(img,SDL_SRCCOLORKEY,SDL_MapRGB(img->format,0,0,0));
		return img;
	}

	SDL_Surface *mapa::leer_paleta(int s){
		int AlP,AnP,i,j,F;
		sprite *aux=paleta;
		frame *aux2;
		SDL_Surface *aux3;
		if(imgs!=NULL)SDL_FreeSurface(imgs);
		AnP=X;
		for(i=1;aux->sig;i++)aux=aux->sig;
		AlP=i*Y;
		imgs=SDL_CreateRGBSurface(SDL_SWSURFACE,AnP,AlP,24,0,0,0,0);
		aux=paleta;
		for(j=0;j<i;j++){
			org.x=0;
			org.y=0;
			org.w=X;
			org.h=Y;
			dest.x=0;
			dest.y=Y*j;
			aux2=aux->frames;
			F=f;
			while((aux2->sig)&&((F--)>0))aux2=aux2->sig;
			aux3=aux2->img;
			SDL_BlitSurface(aux3,&org,imgs,&dest);
			aux=aux->sig;
		}
		return imgs;
	}

	void mapa::operator>>(int F){
		f=F;
	}

	void mapa::guardar(char *name){
		FILE *archivo;
		int i,j,t;
		sprite *aux1=paleta;
		frame *aux2;
		capa *aux3=capas;
		archivo=fopen(name,"wb");
		fputc(x,archivo);
		fputc(X,archivo);
		fputc(y,archivo);
		fputc(Y,archivo);
		while(aux1->sig){
			aux2=aux1->frames;
			while(aux2->sig){
				for(i=0;aux2->nombre[i]!='\0';i++)fputc(aux2->nombre[i],archivo);
				fputc('\"',archivo);
				aux2=aux2->sig;
			}
			//
				for(i=0;aux2->nombre[i]!='\0';i++)fputc(aux2->nombre[i],archivo);
				fputc('\"',archivo);
			//
			fputc(':',archivo);
			aux1=aux1->sig;
		}
		//
			aux2=aux1->frames;
			while(aux2->sig){
				for(i=0;aux2->nombre[i]!='\0';i++)fputc(aux2->nombre[i],archivo);
				fputc('\"',archivo);
				aux2=aux2->sig;
			}
			//
				for(i=0;aux2->nombre[i]!='\0';i++)fputc(aux2->nombre[i],archivo);
				fputc('\"',archivo);
			//
		//
		fputc('>',archivo);
		while(aux3->sig){
			for(i=0;i<x;i++)for(j=0;j<y;j++){
				t=aux3->tabla[i][j].flag;
				putc((t&0x80000000)/0x80000000,archivo);
				putc((t&0x0003fc00)/1024,archivo);
				putc(t&0x800003ff,archivo);
			}
			fputc(':',archivo);
			aux3=aux3->sig;
		}
		//
			for(i=0;i<x;i++)for(j=0;j<y;j++){
				t=aux3->tabla[i][j].flag;
				putc((t&0x80000000)/0x80000000,archivo);
				putc((t&0x0003fc00)/1024,archivo);
				putc(t&0x800003ff,archivo);
			}
		//
		fputc('>',archivo);
		fclose(archivo);
	}

	//constructor carga
	mapa::mapa(char *name){
		FILE *archivo;
		int i,j,t;
		char buf[30],c;
		capa *aux;
		archivo=fopen(name,"rb");
		x=fgetc(archivo);
		X=fgetc(archivo);
		y=fgetc(archivo);
		Y=fgetc(archivo);
		for(i=0;(buf[i]=fgetc(archivo))!='\"';i++);buf[i]='\0';
		paleta=new sprite(buf);
		j=0;
		while((c=fgetc(archivo))!='>'){
			if(c==':'){
				for(i=0;(buf[i]=fgetc(archivo))!='\"';i++);buf[i]='\0';
				addsprite(buf);
				j++;
			}
			else{
				buf[0]=c;
				for(i=1;(buf[i]=fgetc(archivo))!='\"';i++);buf[i]='\0';
				(*this)(j,buf);
			}
		}
		capas=new capa(x,y);
		aux=capas;
		c=0;
		do{
			if(c){addcapa(-1);aux=aux->sig;}
			for(i=0;i<x;i++)for(j=0;j<y;j++){
				t=fgetc(archivo);
				if(t)aux->tabla[i][j].flag|=0x80000000;
				t=fgetc(archivo);
				aux->tabla[i][j].flag|=((t&0x000000ff)*1024)&0x0003fc00;
				t=fgetc(archivo);
				aux->tabla[i][j].flag|=(t&0x000003ff);
			}
			c++;
		}while(fgetc(archivo)==':');
		fclose(archivo);
		img=SDL_CreateRGBSurface(SDL_SWSURFACE,x*X,y*Y,24,0,0,0,0);
		imgb=SDL_CreateRGBSurface(SDL_SWSURFACE,x*X,y*Y,24,0,0,0,0);
		org.x=0;org.y=0;org.w=X;org.h=Y;dest.w=X;dest.h=Y;
		f=0;
		imgs=NULL;
	}

	//funciones de la clase capa

	capa::capa(int x,int y){
		int i;
		sig=NULL;
		X=x;
		tabla=new casilla*[x];
		for(i=0;i<x;i++){
			tabla[i]=new casilla[y];
		}
	}
	capa::~capa(){
		int i;
		if(sig)delete sig;
		for(i=0;i<X;i++){
			delete tabla[i];
		}
		delete tabla;
	}

	//funciones de la clase casilla

	casilla::casilla(){
		flag=0x00000000;
	}

	//funciones de la clase sprite

	sprite::sprite(char *name){
		frames=new frame(name);
		sig=NULL;
	}

	sprite::~sprite(){
		if(sig)delete sig;
		delete frames;
	}

	//funciones de la clase frame

	frame::frame(char *name){
		img=NULL;
		sprintf(nombre,name);
		img=SDL_LoadBMP(name);
		SDL_SetColorKey(img,SDL_SRCCOLORKEY|SDL_RLEACCEL,SDL_MapRGB(img->format,255,255,255));
		sig=NULL;
	}

	frame::~frame(){
		if(sig)delete sig;
		SDL_FreeSurface(img);
	}

}