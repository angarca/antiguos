#ifndef cmapa_h
#define cmapa_h

namespace cmapa{

	class frame;
	class sprite;
	class casilla;
	class capa;

	class mapa{
	private:
		SDL_Rect org,dest;
		SDL_Surface *img,*imgb,*imgs;
		capa *capas;
		sprite *paleta;
		int x,y,X,Y,f;
	public:
		mapa(int Ant,int Alt,int AnM,int Alm);
		mapa(char *name);
		~mapa();
		void addcapa(int p);
		void addsprite(char *name);
		void erasecapa(int c);
		void erasesprite(int s);
		void eraseframe(int s,int F);
		SDL_Surface *operator()(int c);//devuelve la actual imagen del mapa
		SDL_Surface *leer_paleta(int s);
		void operator()(int s,char *name);//a�ade la imagen "name" como ultimo frame del sprite paleta[s] (s>0) 'paleta[0] es imagen predeterminada (trasparente "blanco.bmp")'
		void operator()(int c,int x,int y,int val);
		void operator()(int c,int x,int y,int F,int fase);
		void operator>>(int F);//cambia la fase general por F
		void guardar(char *name);
	};

	class capa{
	private:
		friend mapa;
		casilla **tabla;
		capa *sig;
		int X;
	public:
		capa(int x,int y);
		~capa();
	};

	class casilla{
	private:
		friend capa;
		friend mapa;
		int flag;
	public:
		casilla();
	};

	class sprite{
	private:
		friend mapa;
		frame *frames;
		sprite *sig;
	public:
		sprite(char *name);
		~sprite();
	};

	class frame{
	private:
		friend sprite;
		friend mapa;
		SDL_Surface *img;
		frame *sig;
		char nombre[30];
	public:
		frame(char *name);
		~frame();
	};

}
#endif//cmapa_h