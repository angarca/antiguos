#include <stdio.h>
#include <process.h>
#include <string.h>

int main(int argc,char *argv[]){

	FILE *archivo;
	char buf[100],BUF[100];
	int i;

	sprintf(buf,"%s.txt",argv[1]);

	if(argv[2]){
		archivo=fopen(buf,"w");
		for(i=0;argv[2][i]!='\0';i++)fputc(argv[2][i],archivo);
		fclose(archivo);
	}
	else{
		archivo=fopen(buf,"r");
		for(i=0;(BUF[i]=fgetc(archivo))!=-1;i++);BUF[i]='\0';
		fclose(archivo);
		execl("retorno.exe","retorno.exe",BUF);
	}

	return 0;
}