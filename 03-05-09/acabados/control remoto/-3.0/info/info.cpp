#include <stdio.h>
#include <process.h>

char path[]="versiones/";

int main(int argc,char *argv[]){

	FILE *archivo;
	char buf[100],BUF[100];
	int i;
	sprintf(BUF,"%s%s.txt",path,argv[1]);

	if(argv[2]){
		archivo=fopen(BUF,"w");
		for(i=0;argv[2][i]!='\0';i++)fputc(argv[2][i],archivo);
		fclose(archivo);
	}
	else{
		archivo=fopen(BUF,"r");
		for(i=0;(buf[i]=fgetc(archivo))!=-1;i++);buf[i]='\0';
		fclose(archivo);
		_spawnl( _P_NOWAIT,"retorno.exe","retorno.exe",buf,NULL);
	}

	return 0;
}