#include <stdio.h>

int main(int argc,char *argv[]){

	FILE *org,*dest;
	char c,b=0;

	org=fopen(argv[1],"rb");
	if(org){
		dest=fopen(argv[2],"wb");
		do{
			if(b)fputc(c,dest);
			else b=1;
			c=fgetc(org);
		}while(!feof(org));
		fclose(org);
		fclose(dest);
		remove(argv[1]);
	}

	return 0;

}