#include <SDL.h>
#include <stdio.h>
#include <process.h>

//directorios
char nameo[]="mia/orden.txt";
char named[]="orden.txt";

//clase y funcion de entrada
//	declaracion
class palabra{
private:
	friend palabra *lector(FILE *archivo);
	char *cad;
	palabra *sig;
public:
	palabra(char *buf);
	~palabra();
	char *operator[](int i);
};
//	costructor
palabra::palabra(char *buf){
	int i=0;
	while(buf[i++]!='\0');
	cad=new char[i];
	sprintf(cad,buf);
	sig=NULL;
}
//	destructor
palabra::~palabra(){
	if(cad)delete cad;
	if(sig)delete sig;
}
//	operador
char *palabra::operator [](int i){
	if(i>0)return (*sig)[--i];
	else return cad;
}
//	funcion
palabra *lector(FILE *archivo){
	palabra *raiz,*aux;
	int salida0=1,salida1=1,i;
	char buf[100];
	while(salida0){
		for(i=0;((buf[i]=fgetc(archivo))!=' ')&&(buf[i]!=-1);i++);
		if(buf[i]==-1)salida0=0;
		buf[i]='\0';
		if(salida1){
			raiz=new palabra(buf);
			aux=raiz;
			salida1=0;
		}
		else{
			aux->sig=new palabra(buf);
			aux=aux->sig;
		}
	}
	fclose(archivo);
	return raiz;
}

//main
int main(int argc,char *argv[]){

	int salida0,salida1;
	FILE *archivo;
	palabra *ordenes;
	Sint32 time;

	salida0=1;
	while(salida0){

		//espera ordenes
		salida1=1;
		if((archivo=fopen(named,"r"))){fclose(archivo);remove(named);}
		do{
			//timing
			time=SDL_GetTicks();

			_spawnl( _P_WAIT,"mover.exe","mover.exe",nameo,named,NULL);
			if((archivo=fopen(named,"r"))){
				salida1=0;
			}

			//timing
			if((time=(50-(SDL_GetTicks()-time)))>0)SDL_Delay(time);

		}while(salida1);

		ordenes=lector(archivo);
		remove(named);

		//getion de entrada
		if(!strcmp((*ordenes)[0],"mover")){
			_spawnl(_P_WAIT,"mover.exe","mover.exe",(*ordenes)[1],(*ordenes)[2],NULL);
		}
		if(!strcmp((*ordenes)[0],"abrir")){
			_spawnl(_P_NOWAIT,(*ordenes)[1],(*ordenes)[1],(*ordenes)[2],NULL);
		}
		if(!strcmp((*ordenes)[0],"actualizar")){
			if(!strcmp((*ordenes)[1],"nucleo"))execl("actualizador.exe","actualizador.exe",argv[1],argv[2],NULL);
			_spawnl(_P_NOWAIT,"actualizador.exe","actualizador.exe",argv[1],argv[2],NULL);
		}
		if(!strcmp((*ordenes)[0],"cerrar")){
			salida0=0;
		}

		//para apuntes
	//	if(!strcmp((*ordenes)[0],"")){
	//		_spawnl(_P_WAIT,_P_NOWAIT)
	//		execl()
	//	}

		delete ordenes;

	}


	return 0;
}