#include <stdio.h>

void leer(char *cad,FILE *archivo);
void escrivir(char *cad,FILE *archivo);
int buscar(char c,char *cad);
void arreglar(char *cad);

void main(){

	char cad[30],cad2[30],buf[100],supbuf[1000];
	int enfuncion=0;
	FILE *archivo;
	FILE *salida;

	printf("el archivo : ");
	scanf("%s",&cad);
	sprintf(cad2,"%s.cpp",cad);
	archivo=fopen(cad2,"r");
	sprintf(cad,"%s_debug.cpp",cad);
	salida=fopen(cad,"w");
	sprintf(buf,"#include <stdio.h>\n#include \"debuger.h\"\nchar debuger_buf[100];\n");
	escrivir(buf,salida);

	while(!feof(archivo)){

		leer(buf,archivo);
		escrivir(buf,salida);

		if(enfuncion){
			if(buscar('{',buf))enfuncion++;
			if(buscar('}',buf))enfuncion--;
		}
		else{
			if(buscar('(',buf)&&buscar(')',buf)&&buscar('{',buf))enfuncion=1;
			if(buscar('}',buf))enfuncion=0;
		}

		if(enfuncion){
			arreglar(buf);
			sprintf(supbuf,"sprintf(debuger_buf,\"%s\");\ndebuger::debuger(debuger_buf);\n",buf);
			escrivir(supbuf,salida);
		}

	}

	fclose(archivo);
	fclose(salida);

}

void leer(char *cad,FILE *archivo){
	int i=0;
	do{
		cad[i]=fgetc(archivo);
	}while(cad[i++]!='\n'&&(!feof(archivo)));
	if(feof(archivo))cad[--i]='\0';
	else{
		cad[i]='\0';
	}
}

void escrivir(char *cad,FILE *archivo){
	int i;
	for(i=0;cad[i]!='\0';i++)fputc(cad[i],archivo);
}

int buscar(char c,char *cad){
	int i,b=0;
	for(i=0;cad[i]!='\0';i++)if(c==cad[i])b++;
	return b;
}

void arreglar(char *cad){
	int i=0,j;
	do{
		if(cad[i++]=='\"'){ 
			for(j=0;cad[j++]!='\0';);
			while((j--)>=i)cad[j+1]=cad[j];
			cad[++j]='\\';
			i++;
		}
	}while(cad[i]!='\0');
	cad[i-1]='\\';
	cad[i]='n';
	cad[i+1]='\0';
}