#include <stdio.h>
#include "Cdecision.h"

decision::decision(char *ant){
	int i,j=0,done=0;
	sprintf(antecedente,ant);
	archivo=fopen("partidas.txt","r");
	for(i=0;!feof(archivo);j++){
		tabla[i][j]=fgetc(archivo);
		if(tabla[i][j]=='\n'&&tabla[i][j-1]=='\n'){
			tabla[i][j-1]='\0';
			j=-1;
			i++;
		}
	}
	fclose(archivo);
	indice=i;
	for(i=0;i<9;i++){
		for(j=0;j<4;j++){
			E_casilla[i][j]=0;
		}
	}
}

void decision::calcular(/*debug*/FILE *archivo_debug,char *nombre_debug,int &cuenta_debug/*fin*/){
	int i,j,k;
	//debug
	char num[9][4][15];
	char escritura[1000];
	escritura[0]='\0';
	sprintf(escritura,"%s\n",antecedente);
	//fin
	for(j=0;antecedente[j]!='\0';j++);
	for(i=0;i<indice;i++){
		if(contenido(antecedente,tabla[i])){
			for(k=0;tabla[i][k]!='\0';k++);
			E_casilla[tabla[i][j+3]-'0'][0]++;
			E_casilla[tabla[i][j+3]-'0'][tabla[i][k-1]-'0']++;
		}
	}
	//debug
	sprintf(nombre_debug,"salida_debug%d.txt",cuenta_debug++);
	archivo_debug=fopen(nombre_debug,"w");
	for(i=0;i<9;i++){
		for(j=0;j<4;j++){
			sprintf(num[i][j],"%d",E_casilla[i][j]);
		}
	}
	for(i=0;i<9;i++){
		sprintf(escritura,"%s%d\t%s\t%s\t%s\t%s\n\n",escritura,i,num[i][0],num[i][1],num[i][2],num[i][3]);
	}
	for(i=0;escritura[i]!='\0';i++)fputc(escritura[i],archivo_debug);
	fclose(archivo_debug);
	//fin
}

int decision::decidir(){
	int i,c,min,d=0;
	for(i=0;i<9;i++){
		if(E_casilla[i][0]!=0){
			if(d==0){d=1;min=E_casilla[i][1];c=i;}
			else if(E_casilla[i][1]<min){min=E_casilla[i][1];c=i;}
		}
	}
	return(c+'0');
}

int contenido(char *cad,char *cad2){
	int i;
	for(i=0;cad[i]!='\0';i++){
		if(cad[i]!=cad2[i])return(0);
	}
	return(1);
}