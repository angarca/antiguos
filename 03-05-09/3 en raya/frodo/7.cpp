#include <stdio.h>
#include <stdlib.h>
#define longitud 15

void CrearFosos(char *pista);
void repartir(char *cartas);

void main(){
	int pos1,pos2,partg1=0,partg2=0,done=0,fin,respn,avance;
	char carril1[longitud],carril2[longitud],cartas[6],turno,respc;
	if(rand()%2)turno='1';
	else turno='2';
	printf("CARRERA DE ASTERISCOS\ncomienza el juego\n");
	while(!done){
		sprintf(carril1,"*............\n");
		sprintf(carril2,"*............\n");
		CrearFosos(carril1);
		CrearFosos(carril2);
		pos1=0;
		pos2=0;
		fin=0;
		while(!fin){
			printf("jugador 1 : %s",carril1);
			printf("jugador 2 : %s",carril2);
			printf("  tira el jugador %c\n",turno);
			printf("- - - - - - - - - - - -\n");
			printf("repartimos 5 cartas tapadas escoge un numero del 1 al 5 : ");
			repartir(cartas);
			scanf("%d",&respn);
			printf("\ndestapamos las cartas :%s\n",cartas);
			avance=cartas[respn-1]-'0';
			printf("has escogido avanzar %d posiciones\n",avance);
			if(turno=='1'){
				carril1[pos1]='.';
				pos1+=avance;
				if(pos1>longitud){
					partg1++;
					fin=1;
					printf("el jugador 1 ha llegado al final\n");
					carril1[longitud-3]='*';
			printf("jugador 1 : %s",carril1);
			printf("jugador 2 : %s",carril2);
				}
				else if(carril1[pos1]=='0'){
					partg2++;
					fin=1;
					turno='2';
					printf("el jugador 1 ha caido en un foso\n");
					carril1[pos1]='*';
			printf("jugador 1 : %s",carril1);
			printf("jugador 2 : %s",carril2);
				}
				else{
					carril1[pos1]='*';
					turno='2';
				}
			}
			else{
				carril2[pos2]='.';
				pos2+=avance;
				if(pos2>longitud){
					partg2++;
					fin=1;
					printf("el jugador 2 ha llegado al final\n");
					carril2[longitud-3]='*';
			printf("jugador 1 : %s",carril1);
			printf("jugador 2 : %s",carril2);
				}
				else if(carril2[pos2]=='0'){
					partg1++;
					fin=1;
					turno='1';
					printf("el jugador 2 ha caido en un foso\n");
					carril2[pos2]='*';
			printf("jugador 1 : %s",carril1);
			printf("jugador 2 : %s",carril2);
				}
				else{
					carril2[pos2]='*';
					turno='1';
				}
			}
		}
		printf("ha ganado el jugador : %c\n\n",turno);
		printf("resumen del juego :\njugador 1 ha ganado %d partidas\njugador 2 ha ganado %d partidas\n",partg1,partg2);
		printf("otra partida (s/n)?\n");
		scanf("%c%c",&respc,&respc);
		if(respc=='n')done=1;
	}
}

void CrearFosos(char *pista){
	int i,j,pos;
	for(i=0;pista[i]!='\0';i++);
	for(j=0;j<3;j++){
		pos=(rand()%(i-3))+1;
		if(pista[pos]=='.'){
			pista[pos]='0';
		}
		else j--;
	}
}

void repartir(char *cartas){
	int a,i,j;
	cartas[0]='\0';
	cartas[1]='\0';
	cartas[2]='\0';
	cartas[3]='\0';
	cartas[4]='\0';
	cartas[5]='\0';
	cartas[rand()%5]='1';
	while(cartas[a=rand()%5]);
	cartas[a]='2';
	while(cartas[a=rand()%5]);
	cartas[a]='3';
	while(cartas[a=rand()%5]);
	cartas[a]='4';
	for(i=0;cartas[i];i++);
	cartas[i]='5';
}