#include <stdio.h>
#include <conio.h>

char *mifunc(char  *act);
void minum(char *cad,const char *cado);
void aument(char *cad);
void retorno(char *cad,const char *cado);
int contenido(const char *cad,const char*cad2);
void guardar(const char *cad);

FILE *partidas,*estadisticas;
char tabla[1000000][100];
int indice,superdone=0;


class decision{
private:
	char antecedente[100],salida[500];
	int total,turno,cero[4],uno[4],dos[4],tres[4],cuatro[4],cinco[4],seis[4],siete[4],ocho[4];
public:
	decision(const char *ant);
	char *leer();
	void calcular();
};

decision::decision(const char *ant){
	int i;
	for(i=0;ant[i]!='\0';i++)antecedente[i]=ant[i];
	antecedente[i]='\0';
	for(i=0;i<4;i++){
		cero[i]=0;
		uno[i]=0;
		dos[i]=0;
		tres[i]=0;
		cuatro[i]=0;
		cinco[i]=0;
		seis[i]=0;
		siete[i]=0;
		ocho[i]=0;
	};
}
char *decision::leer(){
	sprintf(salida,"%s\n\t0 - %d\t\t1 - %d\t\t2 - %d\t\t3 - %d\t\t4 - %d\n\t%d %d %d\t%d %d %d\t%d %d %d\t%d %d %d\t%d %d %d\n\t5 - %d\t\t6 - %d\t\t7 - %d\t\t8 - %d\n\t%d %d %d\t%d %d %d\t%d %d %d\t%d %d %d\n\n\n",antecedente,cero[0],uno[0],dos[0],tres[0],cuatro[0],cero[1],cero[2],cero[3],uno[1],uno[2],uno[3],dos[1],dos[2],dos[3],tres[1],tres[2],tres[3],cuatro[1],cuatro[2],cuatro[3],cinco[0],seis[0],siete[0],ocho[0],cinco[1],cinco[2],cinco[3],seis[1],seis[2],seis[3],siete[1],siete[2],siete[3],ocho[1],ocho[2],ocho[3]);
	return(salida);
}
void decision::calcular(){
	int i,j,k;
	for(k=0;antecedente[k]!='\0';k++);
	for(i=0;i<indice;i++){
		if(contenido(antecedente,tabla[i])){
			for(j=0;tabla[i][j]!='\0';j++);
			if(tabla[i][k+3]=='0'){
				cero[0]++;
				cero[(tabla[i][j-1]-'0')]++;
			};
			if(tabla[i][k+3]=='1'){
				uno[0]++;
				uno[(tabla[i][j-1]-'0')]++;
			};
			if(tabla[i][k+3]=='2'){
				dos[0]++;
				dos[(tabla[i][j-1]-'0')]++;
			};
			if(tabla[i][k+3]=='3'){
				tres[0]++;
				tres[(tabla[i][j-1]-'0')]++;
			};
			if(tabla[i][k+3]=='4'){
				cuatro[0]++;
				cuatro[(tabla[i][j-1]-'0')]++;
			};
			if(tabla[i][k+3]=='5'){
				cinco[0]++;
				cinco[(tabla[i][j-1]-'0')]++;
			};
			if(tabla[i][k+3]=='6'){
				seis[0]++;
				seis[(tabla[i][j-1]-'0')]++;
			};
			if(tabla[i][k+3]=='7'){
				siete[0]++;
				siete[(tabla[i][j-1]-'0')]++;
			};
			if(tabla[i][k+3]=='8'){
				ocho[0]++;
				ocho[(tabla[i][j-1]-'0')]++;
			};
		};
	};
}

char *mifunc(char *act){
	int i,done=0;
	char cad[100];
	while(!done){
		if(act[0]=='\0'){
			act[0]='1';
			act[1]='\0';
		}
		else if(act[1]=='\0'){
			act[1]=' ';
			act[2]='-';
			act[3]=' ';
			act[4]='0';
			act[5]='\n';
			act[6]='2';
			act[7]='\0';
		}
		else{
			minum(cad,act);
			aument(cad);
			for(i=0;cad[i]!='\0';i++);
			if(i>13){done=1;superdone=1;}
			retorno(act,cad);
		}
		for(i=0;i<indice;i++)if(contenido(act,tabla[i])){done=1;i=indice;};
	};
	return(act);
}

void minum(char *cad,const char *cado){
	int i,j;
	for(j=0;cado[j]!='\0';j++);
	for(i=4;i<j;i+=6)cad[(i-4)/6]=cado[i];
	cad[(i-4)/6]='\0';
}

void aument(char *cad){
	int i,j=1,k,done=0;
	for(i=0;cad[i]!='\0';i++);
	while(!done){
		if(cad[i-j]<'8'){
			cad[i-j]++;
			for(k=(i-j)+1;k<i;k++){
				cad[k]='0';
			};
			done=1;
		}
		else{j++;}
		if(j>i){
			for(j=0;j<=i;j++){
				cad[j]='0';
			};
			cad[j]='\0';
			done=1;
		};
	};
}

void retorno(char *cad,const char *cado){
	int i,j;
	for(j=0;cad[j]!='\0';j++);
	for(i=0;cado[i]!='\0';i++)cad[i*6+4]=cado[i];
	if(((--i)*6+4)>j){
		cad[j++]=' ';
		cad[j++]='-';
		cad[j++]=' ';
		j++;
		cad[j++]='\n';
		if(cad[j-6]=='1')cad[j++]='2';
		else cad[j++]='1';
		cad[j]='\0';
	};
}

int contenido(const char *cad,const char *cad2){
	int i=0;
	while(cad[i]!='\0'){
		if(cad[i]!=cad2[i])return(0);
		i++;
	};
	return(1);
}

void guardar(const char *cad){
	int i;
	estadisticas=fopen("estadisticas.txt","a");
	for(i=0;cad[i]!='\0';i++)fputc(cad[i],estadisticas);
	fclose(estadisticas);
}

void main(){
	decision *actual;
	int i,j=0;
	char nombre[100]="";
	partidas=fopen("partidas.txt","r");
	for(i=0;!feof(partidas);j++){
		tabla[i][j]=fgetc(partidas);
		if(tabla[i][j]=='\n'&&tabla[i][j-1]=='\n'){
			tabla[i][j-1]='\0';
			j=-1;
			i++;
		};
	};
	indice=i;
	fclose(partidas);
	while(!superdone){
		actual=new(decision)(mifunc(nombre));
		actual->calcular();
		guardar(actual->leer());
		delete(actual);
	};
}