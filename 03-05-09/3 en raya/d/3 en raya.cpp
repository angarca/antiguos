#include <stdio.h>
#include <conio.h>


//declaraciones
int tablero[3][3];

class jugada{
private:
	int disponibles[9];
public:
	int jugador;
	int casilla;
	jugada *siguiente;
	jugada *anterior;
	jugada(jugada *ant);
	jugada *jugar(int turno);
};

jugada::jugada(jugada *ant){
	anterior=ant;
	for(int i=0;i<3;i++){
		for(int j=0;j<3;j++){
			if(tablero[i][j]==0){
				disponibles[(i*3+j)]=1;
			}else{
				disponibles[(i*3+j)]=0;
			};
		};
	};
	siguiente=NULL;
}

jugada *jugada::jugar(int turno){
	for(int i=0;i<9;i++)if(disponibles[i]==1){
		jugador=turno;
		casilla=i;
		disponibles[i]=0;
		tablero[i/3][i%3]=turno;
		siguiente=new(jugada)(this);
		i=9;
	};
	return(siguiente);
}


jugada *retroceso(jugada *act){
	jugada *ant;
	ant=act->anterior;
	delete(act);
	ant->siguiente=NULL;
	tablero[ant->casilla/3][ant->casilla%3]=0;
	return(ant);
}


struct partida{
	jugada *primera;
	int ganador;
}juego;


void ini(){
	for(int i=0;i<3;i++){
		for(int j=0;j<3;j++){
			tablero[i][j]=0;
		};
	};
	juego.primera=new(jugada)(NULL);
}


void guardar(int num){
	FILE *archivo;
	jugada *actual;
//	char nombre[25];
	actual=juego.primera;
//	sprintf(nombre,"partida_num_%d.txt",num);
//	archivo=fopen(nombre,"w");
	archivo=fopen("partidas.txt","a");
	while(actual->siguiente!=0){
		fputc(actual->jugador+'0',archivo);
		fputc(' ',archivo);
		fputc('-',archivo);
		fputc(' ',archivo);
		fputc(actual->casilla+'0',archivo);
		fputc('\n',archivo);
		actual=actual->siguiente;
	};
	fputc(juego.ganador+'0',archivo);
	fputc('\n',archivo);
	fputc('\n',archivo);
	fclose(archivo);
}

int tres(){
	for(int i=0;i<3;i++){
		if(tablero[i][0]==tablero[i][1]&&tablero[i][1]==tablero[i][2])return(tablero[i][0]);
	};
	for(i=0;i<3;i++){
		if(tablero[0][i]==tablero[1][i]&&tablero[1][i]==tablero[2][i])return(tablero[0][i]);
	};
	if(tablero[0][0]==tablero[1][1]&&tablero[1][1]==tablero[2][2])return(tablero[1][1]);
	if(tablero[2][0]==tablero[1][1]&&tablero[1][1]==tablero[0][2])return(tablero[1][1]);
	return(0);
}


int tablas(){
	for(int i=0;i<9;i++){
		if(tablero[i/3][i%3]==0)return(0);
	};
	return(1);
}


void main(){
	int num=0,a,turno=1,done=0;
	jugada *actual,*b;
	ini();
	actual=juego.primera;
	while(!done){
		if(a=tres()){
			juego.ganador=a;
			guardar(num++);
			actual=retroceso(actual);
			if(turno==1)turno=2;
			else turno=1;
		};
		if((b=actual->jugar(turno))==NULL){
			if(tablas()){
				juego.ganador=3;
				guardar(num++);
			};
			if(actual->anterior)b=retroceso(actual);
			else done=1;
		};
		actual=b;
		if(turno==1)turno=2;
		else turno=1;
	};
}
