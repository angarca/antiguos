#include <stdio.h>

int potencia(int base,int exponente){
	int acumulador=1;
	for(;exponente>0;exponente--){
		acumulador*=base;
	};
	return(acumulador);
}

int mifunc(int num){
	int sum=0;
	for(;num>=0;num--){
		sum+=potencia(2,num);
	};
	return(sum);
}




void main(){
	printf("%d\n%d\n%d\n%d\n%d\n%d\n%d",potencia(2,30),potencia(2,31),potencia(2,32),potencia(2,33),(potencia(2,31)-1),(potencia(2,32)-1),mifunc(30));
}