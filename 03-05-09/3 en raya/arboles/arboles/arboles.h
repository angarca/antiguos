#ifndef arboles_h
#define arboles_h

namespace arboles{

	class arbol{
	private:
		nodo *raiz;
		nodo *posicion;
		int tama�o;
		int BL[100];
	public:
		arbol(int t);
		arbol(arbol *original);
		void *operator[](int i);
		void *operator()(int i);
		void operator<<(int i);
		void operator>>(int i);
		void addnodo(int cantidad);
	};

	class nodo{
	private:
		friend arbol;
		void *posicion;
		nodo *sig;
		int cantidad;
	public:
		nodo(int cant);
	};

}//namespace arboles

#endif//arbolse_h