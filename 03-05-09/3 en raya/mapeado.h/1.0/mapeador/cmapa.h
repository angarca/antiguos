#ifndef cmapa_h
#define cmapa_h

namespace cmapa{//para la clase mapa y sus adyacentes
	struct Sprite{//para crear una lista enlazada con los sprites de la paleta
		sprite *SPrite;//el sprite concreto
		Sprite *sig;//el siguiente de la lista
		Sprite(sprite *S);//asigna sprite y nulifica sig
	};

	class mapa{//la clase principal
	private:
		unsigned int fase,ncapas,x,y,z;//la fase general para movimientos ciclicos ordinados (durante la ejecucion)
							////la cantidad de capas actual del mapa
							////el ancho del mapa en cantidad de casillas
							////el alto del mapa en canidad de casillas
							////la cantidad de sprites actualmente en la paleta	
		tabla *capas;//la raiz de la lista enlazada de capas
		Sprite *paleta;//la raiz de la lista enlazada de sprites
	public:
		mapa(int mx,int my);//contructor pone todo en blanco salvo el tama�o que se le pasa como argumento y que nunca es modificado
		tabla *operator[](int i);//para acceder a una de las tablas de la lista
		void addcapa();//para a�adir una capa (en blanco, tras la ultima existente)
		void addsprite(sprite *S);//para a�adir un sprite a la paleta apartir del argumento (en ultimo lugar)
	};

	class casilla{//cada casilla del mapa
	private:
		int flag;//int de 32 bits subdividido para ahorrar espacio
				///1 _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ f _ _ _ s _ _ _ _ _ _ _ _ _
				///1 bit para bandera de control de fase general o individual
				///f _ _ _ 0-15 para marcar la fase actual en caso de fase individual
				///s... 0-1023 para marcar el sprite de la paleta que ocupa la casilla 
	public:
		casilla();//contrutor coloca el flag en blanco 0x00000000
		int operator[](int i);//para obtener el valor actual de
							////el sprite [0]
							////fase G/I [1] G-1 I-0
							////fase actual en funcionamiento individual [2]
		void operator()(int i,int v);//para asignar un valor, i identico a [i]; v el valor a asignar, si es mayor del rango se truncaran sus bits
		int operator++();//aumenta fase individual en uno
		int operator++(int);//aumenta fase individual en uno
		int operator--();//decrementa fase individual en uno
		int operator--(int);//decrementa fase individual en uno
		int operator+=(int i);//aumenta fase individual en i
		int operator-=(int i);//decrementa fase individual en i
	};
	
	class tabla{
	private:
		friend mapa;
		casilla **casillas;//array de dos dimensiones que contiene las casillas
		tabla *sig;//siguiente tabla de la lista
	public:
		tabla(int x,int y);//constructor fabrica el entramado de casillas y nulifica sig
	};
	
}//namespace cmapa

#endif//cmapa_h