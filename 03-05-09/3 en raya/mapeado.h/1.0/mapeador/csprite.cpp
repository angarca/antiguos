#include <SDL.h>
#include <stdio.h>
#include "csprite.h"

namespace csprite{

	//funciones clase frame

	frame::frame(){
		img=NULL;
		sig=NULL;
	}

	frame::frame(char *name){
		img=SDL_LoadBMP(name);
		sig=NULL;
		sprintf(NAME,name);
	}

	void frame::load(char *name){
		if(img)unload();
		img=SDL_LoadBMP(name);
		sprintf(NAME,name);
	}

	SDL_Surface *frame::read(){
		return img;
	}

	void frame::unload(){
		if(img)SDL_FreeSurface(img);
		if(sig)delete sig;
	}

	//funciones clase sprite

	void sprite::addframe(frame *img){
		int i;
		frame *act=frames;
		for(i=0;i<nframes;i++)act=act->sig;
		act=new frame(img->NAME);
		nframes++;
	}

	void sprite::addframe(char *name){
		int i;
		if(frames){
			frame *act=frames;
			for(i=1;i<nframes;i++)act=act->sig;
			act->sig=new frame(name);
		}
		else{
			frames=new frame(name);
		}
		nframes++;
	}

	sprite::sprite(){
		frames=NULL;
		nframes=0;
	}

	sprite::sprite(sprite *copia){
		int i;
		frame *act=copia->frames;
		nframes=copia->nframes;
		for(i=0;i<nframes;i++){
			addframe(act);
			act=act->sig;
		}
	}

	void sprite::draw(SDL_Surface *screen,int x,int y,int f){
		int i;
		frame *act=frames;
		SDL_Rect dest;
		dest.x=x;
		dest.y=y;
		for(i=0;i<nframes;i++)act=act->sig;
		SDL_BlitSurface(act->img,NULL,screen,&dest);
	}

	void sprite::guardar(char *name){
		FILE *archivo;
		frame *act=frames;
		char buf[100];
		int i;
		archivo=fopen(name,"wb");
		fputc(nframes,archivo);
		while(act){
			sprintf(buf,"%s ",act->NAME);
			for(i=0;buf[i]!='\0';i++)fputc(buf[i],archivo);
			act=act->sig;
		}
		fclose(archivo);
	}

	void sprite::cargar(char *name){
		FILE *archivo;
		frame *act=NULL;
		char buf[100];
		int i;
		archivo=fopen(name,"rb");
		nframes=fgetc(archivo);
		for(i=0;!feof(archivo);i++){
			buf[i]=fgetc(archivo);
			if(buf[i]==' '){
				buf[i]='\0';
				i=-1;
				if(act==NULL){
					frames=new frame(buf);
					act=frames;
				}
				else{
					act->sig=new frame(buf);
					act=act->sig;
				}
			}
		}
		fclose(archivo);
	}

}//namespace csprite