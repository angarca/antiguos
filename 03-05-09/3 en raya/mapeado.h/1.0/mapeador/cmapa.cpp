#include "csprite.h"
#include "cmapa.h"

namespace cmapa{

	//funciones de la clase casilla

	casilla::casilla(){//costructor
		flag=0x00000000;
	}

	void casilla::operator ()(int i,int v){//asigna un nuevo valor a una de las variables contenidas en flag
		if(i==0){//n� sprite de la casilla guardado en los 10 ultimos bits
			flag&=0xfffffc00;
			flag|=(v&0x000003ff);
		}
		if(i==1){//fase general 1 individual 0  guardado en el primer bit
			if(v){
				flag|=0x80000000;
			}
			else{
				flag&=0x7fffffff;
			}
		}
		if(i==2){//fase actual en caso de individual guardado en los 4 bits siguientes a n� de sprite
			flag&=0xffffc3ff;
			flag|=((v&0x0000000f)*1024);
		}
	}

	int casilla::operator ++(){//aumenta uno la fase actual
		int a;
		a=(flag&0x00003c00)/1024;//a vale la fase actual
		a++;//aunemta a
		flag&=0xffffc3ff;//deja a cero el espafi de la fase actual (imprescindible para que la siguiente linea no genera un valor no valido)
		flag|=((a&0x0000000f)*1024);//asigna el nuevo valor a la fase actual
		return a;//retorna el nuevo valor
	}

	int casilla::operator ++(int){//aumenta uno la fase actual retornando el valor antiguo ^^
		int a;
		a=(flag&0x00003c00)/1024;
		a++;
		flag&=0xffffc3ff;
		flag|=((a&0x0000000f)*1024);
		return --a;
	}

	int casilla::operator +=(int i){//aumenta i la fase actual ^^
		int a;
		a=(flag&0x00003c00)/1024;
		a+=i;
		flag&=0xffffc3ff;
		flag|=((a&0x0000000f)*1024);
		return a;
	}

	int casilla::operator --(){//decrementa uno la fase actual ^^
		int a;
		a=(flag&0x00003c00)/1024;
		a--;
		flag&=0xffffc3ff;
		flag|=((a&0x0000000f)*1024);
		return a;
	}

	int casilla::operator --(int){//decrementa uno la fase actual retornando el valor antiguo ^^
		int a;
		a=(flag&0x00003c00)/1024;
		a--;
		flag&=0xffffc3ff;
		flag|=((a&0x0000000f)*1024);
		return ++a;
	}

	int casilla::operator -=(int i){//decrementa i la fase actual ^^
		int a;
		a=(flag&0x00003c00)/1024;
		a-=i;
		flag&=0xffffc3ff;
		flag|=((a&0x0000000f)*1024);
		return a;
	}

	int casilla::operator [](int i){//lee uno de los valores contenidos en flag
		if(i==0){//n� sprite de la casilla guardado en los 10 ultimos bits
			return (flag&0x000003ff);
		}
		if(i==1){//fase general 1 individual 0  guardado en el primer bit
			return (flag&0x80000000);
		}
		if(i==2){//fase actual en caso de individual guardado en los 4 bits siguientes a n� de sprite
			return (flag&0x00003c00);
		}
		return -1
	}

	//funciones de la clase mapa

	void mapa::addcapa(){
		tabla *act;//para recorrer la lista
		if(capas){//si hay lista
			act=capas;//act apunta a la raiz
			while(act->sig){//recorrer la lista
				act=act->sig;
			}
			act->sig=new tabla(x,y);//crea la nueva capa al final de la lista
		}
		else{//si no ahy lista 
			capas=new tabla(x,y);//crea la nueva capa en la raiz 
		}
	}

	void mapa::addsprite(sprite *S){
		Sprite *act;//para recorrer la lista
		if(paleta){//si ahy lista
			act=paleta;//act apunta a la raiz
			while(act->sig){//recorrer la lista
				act=act->sig;
			}
			act->sig=new Sprite(S);//crea el nuevo Sprite al final de la lista
		}
		else{
			paleta=new Sprite(S);//crea el nuevo Sprite en la raiz
		}
	}

	mapa::mapa(int mx,int my){//constructor
		capas=NULL;
		fase=0;
		ncapas=0;
		paleta=NULL;
		x=mx;
		y=my;
		z=0;
	}

	tabla *mapa::operator [](int i){
		tabla *act//para recorrer la lista
		if(i>=ncapas)i=ncapas-1;//si se pide una capa mayor que la ultima se devolvera la esta
		if(i<0)return -1;//si se a pasado un numero negativo o el numero de capas es cero se devuelve -1 error
		act=capas;//act apunta a la primera capa de la lista [0]
		while(i>0){//recorre la lista hasta llegar a la posicion pedida
			act=act->sig;//se pasa al siguienta de la lista
			i--;//se decrementa el contador
		}
		return act;//retorna la capa pedida
	}

	//constructor de Sprite

	Sprite::Sprite(sprite *S){
		SPrite=new sprite(S);//crea un nuevo sprite con el constructor copia apartir del argumento
		sig=NULL;//nulifica sig
	}

	//funciones de la clase tabla

	tabla::tabla(int x,int y){
		int i,j;
		casillas=new ((*casilla)[x]);
		for(i=0;i<x;i++){
			casillas[i]=new casilla[y];
		}
		sig=NULL;
	}
}//namespace cmapa
