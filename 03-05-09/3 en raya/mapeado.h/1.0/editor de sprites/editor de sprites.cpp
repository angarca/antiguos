#include <stdio.h>
#include <SDL.h>
#include "csprite.h"

int main(int argc, char *argv[]){
	int salir=0,done;
	char c,f;
	char nombre[25];
	csprite::sprite *Sprite;
	while(!salir){
		printf("\nquieres abrir o crear un sprite (a/c)? : ");
		scanf("%c%c",&c,&f);
		Sprite=new csprite::sprite;
		if(c=='a'){
			printf("\nel nombre del archivo por favor : ");
			scanf("%s%c",&nombre,&f);
			Sprite->cargar(nombre);
		}
		done=0;
		while(!done){
			printf("\nquieres agregar frames al sprite (s/n) : ");
			scanf("%c%c",&c,&f);
			if(c=='s'){
				printf("\nel nombre por favor : ");
				scanf("%s%c",&nombre,&f);
				Sprite->addframe(nombre);
			}
			else
			{
				done=1;
				printf("\nquieres guardar los cambios (s/n)? : ");
				scanf("%c%c",&c,&f);
				if(c=='s'){
					printf("\nel nombre por favor : ");
					scanf("%s%c",&nombre,&f);
					Sprite->guardar(nombre);
				}
			}
		}
		printf("\nquieres salir (s/n)? : ");
		scanf("%c%c",&c,&f);
		if(c=='s')salir=1;
	}
	if(Sprite)delete Sprite;
	return(0);
}
