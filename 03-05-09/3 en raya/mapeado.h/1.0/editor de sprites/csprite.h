#ifndef csprite_h
#define csprite_h

namespace csprite{

	class sprite;//predafinicion para friend

	class frame{//cada imagen del sprite(forma enlazada de frames)
	private:
		friend sprite;
		SDL_Surface *img;//imagen del frame
		frame *sig;//siguiente de la lista (que compone el sprite)
		char NAME[25];//nombre del archivo del que se cargo la imagen
	public:
		frame();//constructor en blanco nulifica los tres punteros
		frame(char *name);//constructor a partir de nombre (carga la imagen y guarda el nombre; nulifica sig
		void load(char *name);//carga imagen y guarda el nombre
		SDL_Surface *read();//devuelve la superficie que contiene la imagen
		void unload();//libera los punteros
	};

	class sprite{//lista enlazada de frames
	private:
		frame *frames;//puntero raiz de la lista
		int nframes;//n� de elementos de la lista
	public:
		sprite();//constructor en blanco nulifica frames y pone nframes a 0
		sprite(sprite *copia);//constructor copia
		void addframe(frame *img);//a�ade un frame al final de la lista
		void addframe(char *name);//a�ade un frame al final de la lista
		void draw(SDL_Surface *scree,int x,int y,int f);//dibuja el frame especificado en screen a partir de x,y
		void guardar(char *name);
		void cargar(char *name);
	};

}//namespace csprite

#endif//csprite_h