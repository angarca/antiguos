#include <stdio.h>
#include <stdlib.h>
#include <SDL.h>
#include "mapeado.h"

int main(int argc, char *argv[]){
	SDL_Surface *pantalla;
	SDL_Rect dest;
	SDL_Event event;
	int done=0;
// Iniciar SDL
if (SDL_Init(SDL_INIT_VIDEO) < 0) {
printf("No se pudo iniciar SDL: %s\n",SDL_GetError());
exit(1);
}
// Activamos modo de video
pantalla = SDL_SetVideoMode(640,640,24,SDL_HWSURFACE);
if (pantalla == NULL) {
printf("No se puede inicializar el modo gr�fico: \n",SDL_GetError());
exit(1);
}
atexit(SDL_Quit);
	mapa map;
	map.addtile("casilla.bmp");
	dest.x=0;
	dest.y=0;
	SDL_BlitSurface(map.read(),NULL,pantalla,&dest);
	SDL_Flip(pantalla);
// Esperamos la pulsaci�n de una tecla para salir
while(done == 0) {
while ( SDL_PollEvent(&event) ) {
if ( event.type == SDL_KEYDOWN )
done = 1;
}
}
	return 0;
}