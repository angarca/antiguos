#include <SDL.h>
#include <stdio.h>
#include "mapeado.h"

#ifndef profundidad_de_color
#define profundidad_de_color 32
#endif//profundidad_de_color
#ifndef mascaras_de_color
#define mascaras_de_color
#define rmask 0xff000000
#define gmask 0x00ff0000
#define bmask 0x0000ff00
#define amask 0x000000ff
#endif//mascaras_de_color

tile::tile(char *name){
	int i;
	if(img=SDL_LoadBMP(name)){
		for(i=0;name[i]!='\0';i++);
		nombre=new char[i+1];
		sprintf(nombre,name);
	}else{
		name=NULL;
	}
	sig=NULL;
}

mapa::mapa(int al,int an,int aldt,int andt){
	alto=al;ancho=an;AltoDelTile=aldt;AnchoDelTile=andt;
	chipset=NULL;
	screen=NULL;
	map=new int*[alto];
	for(int i=0;i<alto;i++){
		map[i]=new int[ancho];
	}
	for(i=0;i<al;i++){
		for(int j=0;j<an;j++){
			map[i][j]=0;
		}
	}
	ntiles=0;
}

mapa::mapa(int al,int an){
	alto=al;ancho=an;AltoDelTile=64;AnchoDelTile=64;
	chipset=NULL;
	screen=NULL;
	map=new int*[alto];
	for(int i=0;i<alto;i++){
		map[i]=new int[ancho];
	}
	for(i=0;i<al;i++){
		for(int j=0;j<an;j++){
			map[i][j]=0;
		}
	}
	ntiles=0;
}

mapa::mapa(){
	alto=10;ancho=10;AltoDelTile=64;AnchoDelTile=64;
	chipset=NULL;
	screen=NULL;
	map=new int*[10];
	for(int i=0;i<10;i++){
		map[i]=new int[10];
	}
	for(i=0;i<alto;i++){
		for(int j=0;j<ancho;j++){
			map[i][j]=0;
		}
	}
	ntiles=0;
}

void mapa::addtile(char *name){
	if(chipset){
		for(tile *uno=chipset;uno->sig;uno=uno->sig);
		if(uno->sig=new tile(name)){
			if(uno->sig->img==NULL){
				delete uno->sig;
				uno->sig=NULL;
			}
			else ntiles++;
		}
	}
	else{
		if(chipset=new tile(name)){
			if(chipset->img==NULL){
				delete chipset;
				chipset=NULL;
			}
			else ntiles++;
		}
	}
}

void mapa::asignar(int y,int x,int img){
	if(img>=0&&img<ntiles)map[y][x]=img;
}

SDL_Surface *mapa::read(){
	if(ntiles<1) screen=NULL;
	else{
		if(screen) SDL_FreeSurface(screen);
		if(screen=SDL_CreateRGBSurface(SDL_SWSURFACE,ancho*AnchoDelTile,alto*AltoDelTile,profundidad_de_color,rmask,gmask,bmask,amask)){
			SDL_Rect dest,sal;
			sal.x=0;
			sal.y=0;
			sal.h=AltoDelTile;
			sal.w=AnchoDelTile;
			dest.w=AnchoDelTile;
			dest.h=AltoDelTile;
			for(int i=0;i<alto;i++){
				dest.y=i*AltoDelTile;
				for(int j=0;j<ancho;j++){
					tile *uno=chipset;
					for(int k=0;uno->sig&&k<map[i][j];k++)uno=uno->sig;
					dest.x=j*AnchoDelTile;
					SDL_BlitSurface(uno->img,&sal,screen,&dest);
				}
			}
		}
		else screen=NULL;
	}
	return screen;
}