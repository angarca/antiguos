#include <stdio.h>
#include <stdlib.h>
#include <SDL.h>
#include <windows.h>
#include "mapeado.h"

void iniciar(int ancho,int alto);
void alsalir();
//debug
void salir(char *cad);
//fin

//struct cursor
struct sprite{
	tile *img;
	int x,y;
	sprite(){
		img=new tile("cursor.bmp");
		x=0;
		y=0;
	};
}cursor;


//punteros globales
SDL_Surface *screen;
mapa *map;
//debug
FILE *salida_debug;
int estvid=0;
//fin

int main(int argc, char *argv[]){
	int ancho,alto,anchot,altot;
	char c,nombre[25];
	int done=0,vdone=0;
	SDL_Event event;
	SDL_Rect dest,dcursor;
	dest.x=0;
	dest.y=0;
	atexit(alsalir);
	printf("\nquieres abrir un mapa o crear uno nuevo 'a'/'c'?\n");
	scanf("%c",&c);
	if(c=='a'){
	}
	else{
		printf("\nque tama�o de tiles quieres 'ancho' 'alto' ?\n");
		scanf("%d%d",&anchot,&altot);
		printf("\ncuantos tiles quieres en el mapa 'ancho' 'alto' ?\n");
		scanf("%d%d",&ancho,&alto);
		map=new mapa(alto,ancho,altot,anchot);
	}
	while(!done){
		printf("\nquieres modificar el mapa o salir 'm'/'s' ?\n");
		scanf("%c%c",&c,&c);
		if(c=='m'){
			printf("\nquieres a�adir un tile 'y'/'n' ?\n");
			scanf("%c%c",&c,&c);
			while(c=='y'){
				printf("\nel nombre por favor 'nombre' ?\n");
				scanf("%c%s",&c,&nombre);
				map->addtile(nombre);
				printf("\nquieres a�adir otro tile 'y'/'n' ?\n");
				scanf("%c%c",&c,&c);
			}
			iniciar(ancho*anchot,alto*altot);estvid=1;
			vdone=0;
			while(!vdone){
				SDL_BlitSurface(map->read(),NULL,screen,&dest);
				dcursor.x=anchot*cursor.x;
				dcursor.y=altot*cursor.y;
				SDL_BlitSurface(cursor.img->img,NULL,screen,&dcursor);
				SDL_Flip(screen);
				c=0;
				while(!c){
					SDL_PollEvent(&event);
					if(event.type==SDL_KEYDOWN){
						switch(event.key.keysym.sym){
						case SDLK_UP:
							if(cursor.y>0)cursor.y--;
							break;
						case SDLK_DOWN:
							if(cursor.y<alto-1)cursor.y++;
							break;
						case SDLK_LEFT:
							if(cursor.x>0)cursor.x--;
							break;
						case SDLK_RIGHT:
							if(cursor.x<ancho-1)cursor.x++;
							break;
						case SDLK_0:
							map->asignar(cursor.y,cursor.x,0);
							break;
						case SDLK_1:
							map->asignar(cursor.y,cursor.x,1);
							break;
						case SDLK_2:
							map->asignar(cursor.y,cursor.x,2);
							break;
						case SDLK_3:
							map->asignar(cursor.y,cursor.x,3);
							break;
						case SDLK_4:
							map->asignar(cursor.y,cursor.x,4);
							break;
						case SDLK_5:
							map->asignar(cursor.y,cursor.x,5);
							break;
						case SDLK_n:
							vdone=1;
						}
						c=1;
						Sleep(100);
					}
				}
			}
			SDL_FreeSurface(screen);
			SDL_Quit();estvid=0;
		}
		else done=1;
	}
	return 0;
}

void iniciar(int ancho,int alto){
	/*debug*/if(/*fin*/SDL_Init(SDL_INIT_VIDEO)/*debug*/<0){
		salir(SDL_GetError());
	}//fin
	screen=SDL_SetVideoMode(ancho,alto,24,SDL_HWSURFACE);
	//debug
	if(screen==NULL)salir(SDL_GetError());
	//fin
}

void alsalir(){
	if(map)delete map;
	if(estvid)SDL_Quit();
}

//debug
void salir(char *cad){
	salida_debug=fopen("salida_debug.txt","w");
	for(int i=0;cad[i]!='\0';i++)fputc(cad[i],salida_debug);
	exit(1);
}
//fin