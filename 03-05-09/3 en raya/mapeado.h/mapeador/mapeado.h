#ifndef mapeado_h
#define mapeado_h

struct tile{
	SDL_Surface *img;
	char *nombre;
	tile *sig;
	tile(char *name);
};

class mapa{
private:
	int alto,ancho;
	int AltoDelTile,AnchoDelTile;
	tile *chipset;
	int ntiles;
	int **map;
	SDL_Surface *screen;
public:
	mapa();
	mapa(int al,int an);
	mapa(int al,int an,int aldt,int andt);
	void addtile(char *name);
	void asignar(int x,int y,int img);
	SDL_Surface *read();
};

#endif//mapeado_h