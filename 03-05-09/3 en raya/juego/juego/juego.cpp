#include <stdio.h>
#include <SDL.h>
#include "csprite.h"

int contenido(const char *cad,const char*cad2);
void drawscene(SDL_Surface *screen);
int initsprites();
void mifunc(char *cad);
int tres();
int tablas();

FILE *partidas;
char tabla[1000000][100];
char tablero[3][3];
int indice;
CFrame blanco;
CFrame equis;
CFrame circulo;
CSprite suelo[3];
SDL_Surface *screen;


class decision{
private:
	char antecedente[100];//,salida[500];
public:
	int cero[4],uno[4],dos[4],tres[4],cuatro[4],cinco[4],seis[4],siete[4],ocho[4];
	decision(const char *ant);
//	char *leer();
	void calcular();
}*actual;

decision::decision(const char *ant){
	int i;
	for(i=0;ant[i]!='\0';i++)antecedente[i]=ant[i];
	antecedente[i]='\0';
	for(i=0;i<4;i++){
		cero[i]=0;
		uno[i]=0;
		dos[i]=0;
		tres[i]=0;
		cuatro[i]=0;
		cinco[i]=0;
		seis[i]=0;
		siete[i]=0;
		ocho[i]=0;
	};
}
//char *decision::leer(){
//	sprintf(salida,"%s\n\t0 - %d\t\t1 - %d\t\t2 - %d\t\t3 - %d\t\t4 - %d\n\t%d %d %d\t%d %d %d\t%d %d %d\t%d %d %d\t%d %d %d\n\t5 - %d\t\t6 - %d\t\t7 - %d\t\t8 - %d\n\t%d %d %d\t%d %d %d\t%d %d %d\t%d %d %d\n\n\n",antecedente,cero[0],uno[0],dos[0],tres[0],cuatro[0],cero[1],cero[2],cero[3],uno[1],uno[2],uno[3],dos[1],dos[2],dos[3],tres[1],tres[2],tres[3],cuatro[1],cuatro[2],cuatro[3],cinco[0],seis[0],siete[0],ocho[0],cinco[1],cinco[2],cinco[3],seis[1],seis[2],seis[3],siete[1],siete[2],siete[3],ocho[1],ocho[2],ocho[3]);
//	return(salida);
//}
void decision::calcular(){
	int i,j,k;
	for(k=0;antecedente[k]!='\0';k++);
	for(i=0;i<indice;i++){
		if(contenido(antecedente,tabla[i])){
			for(j=0;tabla[i][j]!='\0';j++);
			if(tabla[i][k+3]=='0'){
				cero[0]++;
				cero[(tabla[i][j-1]-'0')]++;
			};
			if(tabla[i][k+3]=='1'){
				uno[0]++;
				uno[(tabla[i][j-1]-'0')]++;
			};
			if(tabla[i][k+3]=='2'){
				dos[0]++;
				dos[(tabla[i][j-1]-'0')]++;
			};
			if(tabla[i][k+3]=='3'){
				tres[0]++;
				tres[(tabla[i][j-1]-'0')]++;
			};
			if(tabla[i][k+3]=='4'){
				cuatro[0]++;
				cuatro[(tabla[i][j-1]-'0')]++;
			};
			if(tabla[i][k+3]=='5'){
				cinco[0]++;
				cinco[(tabla[i][j-1]-'0')]++;
			};
			if(tabla[i][k+3]=='6'){
				seis[0]++;
				seis[(tabla[i][j-1]-'0')]++;
			};
			if(tabla[i][k+3]=='7'){
				siete[0]++;
				siete[(tabla[i][j-1]-'0')]++;
			};
			if(tabla[i][k+3]=='8'){
				ocho[0]++;
				ocho[(tabla[i][j-1]-'0')]++;
			};
		};
	};
}


int contenido(const char *cad,const char *cad2){
	int i=0;
	while(cad[i]!='\0'){
		if(cad[i]!=cad2[i])return(0);
		i++;
	};
	return(1);
}

int main(){
	SDL_Event event;
	int i,j=0,done=0,a=0,turno=1;
	char nombre[100]="";
	partidas=fopen("partidas.txt","r");
	for(i=0;!feof(partidas);j++){
		tabla[i][j]=fgetc(partidas);
		if(tabla[i][j]=='\n'&&tabla[i][j-1]=='\n'){
			tabla[i][j-1]='\0';
			j=-1;
			i++;
		};
	};
	indice=i;
	fclose(partidas);
	for(i=0;i<3;i++){for(j=0;j<3;j++){tablero[i][j]=0;};};
	if (SDL_Init(SDL_INIT_VIDEO) < 0) {
		printf("No se pudo iniciar SDL: %s\n",SDL_GetError());
		return(1);
	}
	screen = SDL_SetVideoMode(192,192,24,SDL_HWSURFACE);
	if (screen == NULL) {
		printf("No se puede inicializar el modo gr�fico: \n",SDL_GetError());
		return(1);
	}
	atexit(SDL_Quit);
	initsprites();
	while(!done){
		drawscene(screen);
		if(turno==1){
			while(!a){
				if(SDL_PollEvent(&event)){
					if(event.type==SDL_KEYDOWN){
						if(event.key.keysym.sym==SDLK_0){
							tablero[0][0]=1;
							sprintf(nombre,"%s1 - 0\n2",nombre);
						}
						if(event.key.keysym.sym==SDLK_1){
							tablero[0][1]=1;
							sprintf(nombre,"%s1 - 1\n2",nombre);
						}
						if(event.key.keysym.sym==SDLK_2){
							tablero[0][2]=1;
							sprintf(nombre,"%s1 - 2\n2",nombre);
						}
						if(event.key.keysym.sym==SDLK_3){
							tablero[1][0]=1;
							sprintf(nombre,"%s1 - 3\n2",nombre);
						}
						if(event.key.keysym.sym==SDLK_4){
							tablero[1][1]=1;
							sprintf(nombre,"%s1 - 4\n2",nombre);
						}
						if(event.key.keysym.sym==SDLK_5){
							tablero[1][2]=1;
							sprintf(nombre,"%s1 - 5\n2",nombre);
						}
						if(event.key.keysym.sym==SDLK_6){
							tablero[2][0]=1;
							sprintf(nombre,"%s1 - 6\n2",nombre);
						}
						if(event.key.keysym.sym==SDLK_7){
							tablero[2][1]=1;
							sprintf(nombre,"%s1 - 7\n2",nombre);
						}
						if(event.key.keysym.sym==SDLK_8){
							tablero[2][2]=1;
							sprintf(nombre,"%s1 - 8\n2",nombre);
						}
					}
				}
			}
			turno=2;
		}
		else{
			mifunc(nombre);
			turno=1;
		}
		a=0;
		if(tres())done=1;
		if(tablas())done=1;
//		if(done==1){
//		}
	};
	suelo[0].finalize();
	suelo[1].finalize();
	suelo[2].finalize();
	SDL_FreeSurface(screen);
	return(0);
}

void drawscene(SDL_Surface *screen){
	int i,j,t,x,y;
	for(i=0;i<3;i++){
		for(j=0;j<3;j++){
			t=tablero[i][j];
			x=j*64;
			y=i*64;
			suelo[t].setx(x);
			suelo[t].sety(y);
			suelo[t].draw(screen);
		};
	};
}

int initsprites(){
	blanco.load("casilla.bmp");
	suelo[0].addframe(blanco);

	equis.load("equis.bmp");
	suelo[1].addframe(equis);

	circulo.load("circulo.bmp");
	suelo[2].addframe(circulo);

	return (0);
}

void mifunc(char *cad){
	int max=0,m;
	actual=new(decision)(cad);
	actual->calcular();
	if(actual->cero[1]>max){max=actual->cero[1];m=0;}
	if(actual->uno[1]>max){max=actual->uno[1];m=1;}
	if(actual->dos[1]>max){max=actual->dos[1];m=2;}
	if(actual->tres[1]>max){max=actual->tres[1];m=3;}
	if(actual->cuatro[1]>max){max=actual->cuatro[1];m=4;}
	if(actual->cinco[1]>max){max=actual->cinco[1];m=5;}
	if(actual->seis[1]>max){max=actual->seis[1];m=6;}
	if(actual->siete[1]>max){max=actual->siete[1];m=7;}
	if(actual->ocho[1]>max){max=actual->ocho[1];m=8;}
	delete(actual);
	tablero[m/3][m%3]=2;
	sprintf(cad,"%s - %d\n",cad,m);
}

int tres(){
	for(int i=0;i<3;i++){
		if(tablero[i][0]==tablero[i][1]&&tablero[i][1]==tablero[i][2])return(tablero[i][0]);
	};
	for(i=0;i<3;i++){
		if(tablero[0][i]==tablero[1][i]&&tablero[1][i]==tablero[2][i])return(tablero[0][i]);
	};
	if(tablero[0][0]==tablero[1][1]&&tablero[1][1]==tablero[2][2])return(tablero[1][1]);
	if(tablero[2][0]==tablero[1][1]&&tablero[1][1]==tablero[0][2])return(tablero[1][1]);
	return(0);
}

int tablas(){
	for(int i=0;i<9;i++){
		if(tablero[i/3][i%3]==0)return(0);
	};
	return(1);
}