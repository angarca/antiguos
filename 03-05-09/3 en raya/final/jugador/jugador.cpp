/*****************************************************************
Ejemplo3_1
(C) 2003 by Alberto Garc�a Serrano
Programaci�n de videojuegos con SDL
******************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <SDL.h>
#include "csprite.h"
#include "Cdecision.h"
//declaro mis funciones
void init();
void initgraf();
void drawscene();
int tablas();
int tres();
int dos();
//mis variables
CFrame blanco;
CFrame equis;
CFrame circulo;
CSprite casillas[3];
char tablero[3][3];
SDL_Surface *screen;
int turno;
char nombre[100];
int HeJugado;
decision *una;
int jugada;
int a;
//defino mis funciones
void init(){
	int i,j;
	for(i=0;i<3;i++){
		for(j=0;j<3;j++){
			tablero[i][j]='0';
		}
	}
	turno=1;
	nombre[0]='\0';
}
void initgraf(){
	blanco.load("casilla.bmp");
	casillas[0].addframe(blanco);

	equis.load("equis.bmp");
	casillas[1].addframe(equis);

	circulo.load("circulo.bmp");
	casillas[2].addframe(circulo);
}
void drawscene(){
	int i,j,t;
	for(i=0;i<3;i++){
		for(j=0;j<3;j++){
			t=tablero[i][j]-'0';
			casillas[t].setx(i*64);
			casillas[t].sety(j*64);
			casillas[t].draw(screen);
		}
	}
}
int tablas(){
	int i,j;
	for(i=0;i<3;i++){
		for(j=0;j<3;j++){
			if(tablero[i][j]=='0')return('0');
		}
	}
	return('3');
}
int tres(){
	int i;
	for(i=0;i<3;i++){
		if((tablero[i][0]==tablero[i][1])&&(tablero[i][1]==tablero[i][2])&&(tablero[i][0]!='0'))return(tablero[i][0]);
		if((tablero[0][i]==tablero[1][i])&&(tablero[1][i]==tablero[2][i])&&(tablero[0][i]!='0'))return(tablero[0][i]);
	}
	if((tablero[0][0]==tablero[1][1])&&(tablero[1][1]==tablero[2][2])&&(tablero[0][0]!='0'))return(tablero[0][0]);
	if((tablero[2][0]==tablero[1][1])&&(tablero[1][1]==tablero[0][2])&&(tablero[1][1]!='0'))return(tablero[1][1]);
	return('0');
}
int dos(){
	int i;
	for(i=0;i<3;i++){
		if(tablero[i][0]=='0'&&tablero[i][1]==tablero[i][2])return (i*3);
		if(tablero[i][1]=='0'&&tablero[i][0]==tablero[i][2])return (i*3+1);
		if(tablero[i][2]=='0'&&tablero[i][1]==tablero[i][0])return (i*3+2);
		if(tablero[0][i]=='0'&&tablero[1][i]==tablero[2][i])return (i);
		if(tablero[1][i]=='0'&&tablero[0][i]==tablero[2][i])return (3+i);
		if(tablero[2][i]=='0'&&tablero[1][i]==tablero[0][i])return (6+i);
	}
	if(tablero[0][0]=='0'&&tablero[1][1]==tablero[2][2])return 0;
	if(tablero[1][1]=='0'&&tablero[0][0]==tablero[2][2])return 4;
	if(tablero[2][2]=='0'&&tablero[1][1]==tablero[0][0])return 8;
	if(tablero[2][0]=='0'&&tablero[1][1]==tablero[0][2])return 6;
	if(tablero[1][1]=='0'&&tablero[2][0]==tablero[0][2])return 4;
	if(tablero[0][2]=='0'&&tablero[1][1]==tablero[2][0])return 2;
	return (-1);
}
//main
int main(int argc, char *argv[]) {
	//variables para debug
	FILE *archivo_debug;
	char nombre_debug[25];
	int cuenta_debug=0;
	//fin
	SDL_Surface *image/*, *screen*/;
	SDL_Rect dest;
	SDL_Event event;
	int done = '0';
	atexit(SDL_Quit);
	// Iniciar SDL
	if (SDL_Init(SDL_INIT_VIDEO) < 0) {
		printf("No se pudo iniciar SDL: %s\n",SDL_GetError());
		exit(1);
	}
	// Activamos modo de video
	screen = SDL_SetVideoMode(/*640,480*/192,192/**/,24,SDL_HWSURFACE);
	if (screen == NULL) {
		printf("No se puede inicializar el modo gr�fico: \n",SDL_GetError());
		exit(1);
	}
	//inicializar variables
	init();
	// Cargamos gr�fico
	initgraf();
	image = SDL_LoadBMP("yes_no.bmp");
	if ( image == NULL ) {
		printf("No pude cargar gr�fico: %s\n", SDL_GetError());
		exit(1);
	}
	// Definimos donde dibujaremos el gr�fico
	// y lo copiamos a la pantalla.
	dest.x = 64;
	dest.y = 64;
	dest.w = image->w;
	dest.h = image->h;
//	SDL_BlitSurface(image, NULL, screen, &dest);
//	// Esperamos la pulsaci�n de una tecla para salir
	//gameloop
	while(done == '0') {
		drawscene();
		// Mostramos la pantalla
		SDL_Flip(screen);
		//turno
		if(turno==1){
			//si me toca
			HeJugado=0;
			while(!HeJugado){
				if ( SDL_PollEvent(&event) ) {
					if ( event.type == SDL_KEYDOWN ){
						if(event.key.keysym.sym==SDLK_0){
							if(tablero[0][0]=='0'){
								tablero[0][0]='1';
								sprintf(nombre,"%s1 - 0\n2",nombre);
								HeJugado=1;
							}
						}
						if(event.key.keysym.sym==SDLK_1){
							if(tablero[0][1]=='0'){
								tablero[0][1]='1';
								sprintf(nombre,"%s1 - 1\n2",nombre);
								HeJugado=1;
							}
						}
						if(event.key.keysym.sym==SDLK_2){
							if(tablero[0][2]=='0'){
								tablero[0][2]='1';
								sprintf(nombre,"%s1 - 2\n2",nombre);
								HeJugado=1;
							}
						}
						if(event.key.keysym.sym==SDLK_3){
							if(tablero[1][0]=='0'){
								tablero[1][0]='1';
								sprintf(nombre,"%s1 - 3\n2",nombre);
								HeJugado=1;
							}
						}
						if(event.key.keysym.sym==SDLK_4){
							if(tablero[1][1]=='0'){
								tablero[1][1]='1';
								sprintf(nombre,"%s1 - 4\n2",nombre);
								HeJugado=1;
							}
						}
						if(event.key.keysym.sym==SDLK_5){
							if(tablero[1][2]=='0'){
								tablero[1][2]='1';
								sprintf(nombre,"%s1 - 5\n2",nombre);
								HeJugado=1;
							}
						}
						if(event.key.keysym.sym==SDLK_6){
							if(tablero[2][0]=='0'){
								tablero[2][0]='1';
								sprintf(nombre,"%s1 - 6\n2",nombre);
								HeJugado=1;
							}
						}
						if(event.key.keysym.sym==SDLK_7){
							if(tablero[2][1]=='0'){
								tablero[2][1]='1';
								sprintf(nombre,"%s1 - 7\n2",nombre);
								HeJugado=1;
							}
						}
						if(event.key.keysym.sym==SDLK_8){
							if(tablero[2][2]=='0'){
								tablero[2][2]='1';
								sprintf(nombre,"%s1 - 8\n2",nombre);
								HeJugado=1;
							}
						}
					}
				}
			}
			turno=2;
		}
		else{
			//si le toca
			if((a=dos())!=-1){
				//si hay dos en raya
				tablero[(a-'0')/3][(a-'0')%3]='2';
				turno=1;
				sprintf(nombre,"%s - %c\n",nombre,a);
			}
			else{
				//si no
				una=new(decision)(nombre);
				una->calcular(/*debug*/archivo_debug,nombre_debug,cuenta_debug/*fin*/);
				jugada=una->decidir();
				tablero[(jugada-'0')/3][(jugada-'0')%3]='2';
				sprintf(nombre,"%s - %c\n",nombre,jugada);
				delete(una);
				turno=1;
			}
		}
		//se ha acabado la partida?
		done=tablas();
		if(tres()!='0')done=tres();
		if(done!='0'){
			drawscene();
			SDL_BlitSurface(image, NULL, screen, &dest);
			SDL_Flip(screen);
			HeJugado=0;
			while(!HeJugado){
				if(SDL_PollEvent(&event)){
					if(event.type==SDL_KEYDOWN){
						if(event.key.keysym.sym==SDLK_y){
							init();
							done='0';
							HeJugado=1;
						}
						if(event.key.keysym.sym==SDLK_n){
							HeJugado=1;
						}
					}
				}
			}
		}
	}
	// liberar superficie
	SDL_FreeSurface(image);
	SDL_FreeSurface(screen);
	return 0;
}