#include "tablero.h"

casilla::casilla(){
	estado=0;
}

void casilla::ocupar(int jugador){
	estado=jugador;
}

void tablero::jugar(int x,int y,int jugador){
	casillas[3y+x].ocupar(jugador);
}

int tablero::leer(int x,int y){
	return casillas[3y+x].estado;
}

tablero::tablero(){
	int i;
	casillas=new(casilla[9]);
	for(i=0;i<9;i++){
		casillas[i].x=i%3;
		casillas[i].y=i/3;
	}
}