#ifndef csprite_h
#define csprite_h

class cframe{
private:
	SDL_Surface *img;
	char imagen[50];
	friend class csprite;
	friend class CSprite;
public:
	cframe();
	void load(char *name);
	SDL_Surface *read();
	char *getname();
	void unload();
};

class csprite{
private:
	cframe *sprite;
	int nframes;
	int mframes;
	int frame;
	friend class CSprite;
public:
	csprite(int nf);
	csprite();
	void finalize();
	void addframe(cframe *nframe);
	void delframe(int nf);
	void selframe(int nf);
	void pframe();
	void minframe();
	int frames(char c);
};

class CSprite{
private:
	csprite *sprite;
	int posx,posy;
public:
	void load(csprite *Sprite);
	csprite *read();
	void setx(int x);
	void sety(int y);
	void addx(int x);
	void addy(int y);
	int getx();
	int gety();
	int getw();
	int geth();
	void draw(SDL_Surface *superficie);
	int distx(CSprite *Sprite);
	int disty(CSprite *Sprite);
	int superposicion(CSprite *Sprite);
};
#endif //csprite_h