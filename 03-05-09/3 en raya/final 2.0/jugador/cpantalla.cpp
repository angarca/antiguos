#include <SDL.h>
#include "csprite.h"
#include "ctablero.h"
#include "cpantalla.h"

#ifndef TamaņoDelTile
#define TamaņoDelTile 64
#endif //TamaņoDelTile

pantalla::pantalla(){
	cursor=NULL;
	partida=NULL;
}

void pantalla::addcursor(CSprite *add){
	if(!cursor){
		cursor=new(CSprite);
		cursor->load(add->read());
		cursor->setx(add->getx());
		cursor->sety(add->gety());
	}
}

void pantalla::addpartida(tablero *add){
	partida=add;
}

void pantalla::mostrar(){
}

void pantalla::movcursor(int direc){
	switch(direc){
	case 0:
		cursor->addy(-TamaņoDelTile);
		break;
	case 1:
		cursor->addx(-TamaņoDelTile);
		break;
	case 2:
		cursor->addy(TamaņoDelTile);
		break;
	case 3:
		cursor->addx(TamaņoDelTile);
		break;
	}
}

SDL_Surface *read(){
	return screen;
}