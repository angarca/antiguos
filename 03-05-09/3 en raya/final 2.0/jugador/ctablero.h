#ifndef ctablero_h
#define ctablero_h

class casilla{
private:
	int estado;
	int x,y;
	friend class tablero;
public:
	casilla();
	void ocupar(int jugador);
};


class tablero{
private:
	casilla casillas[9];
public:
	tablero();
	void jugar(int x,int y,int jugador);
	int leer(int x,int y);
};
#endif //ctablero_h