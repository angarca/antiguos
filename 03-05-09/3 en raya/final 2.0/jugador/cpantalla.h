#ifndef cpantalla_h
#define cpantalla_h

class pantalla{
private:
	SDL_Surface *screen;
	tablero *partida;
	CSprite *cursor;
public:
	pantalla();
	void addpartida(tablero *add);
	void addcursor(CSprite *add);
	void movcursor(int direc);
	void mostrar();
	SDL_Surface *read();
};
#endif //cpantalla_h