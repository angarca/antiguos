#include <SDL.h>
#include <stdio.h>
#include "csprite.h"

#ifndef color_T
#define color_T 255,0,0
#endif //color_T

cframe::cframe(){
	img=NULL;
	imagen[0]='\0';
}

char *cframe::getname(){
	return imagen;
}

void cframe::load(char *name){
	sprintf(imagen,name);
	img=SDL_LoadBMP(name);
	SDL_SetColorKey(img,SDL_SRCCOLORKEY|SDL_RLEACCEL, SDL_MapRGB(img->format,color_T));
}

SDL_Surface *cframe::read(){
	return img;
}

void cframe::unload(){
	SDL_FreeSurface(img);
	imagen[0]='\0';
}


void csprite::addframe(cframe *nframe){
	if(mframes<nframes-1){
		sprite[mframes++].load(nframe->getname());
	}
}

csprite::csprite(int nf){
	if(sprite=new cframe[nf]){
		nframes=nf;
		mframes=0;
		frame=-1;
	}
}

csprite::csprite(){
	int nf=1;
	if(sprite=new cframe[nf]){
		nframes=nf;
		mframes=0;
		frame=-1;
	}
}

void csprite::delframe(int nf){
	for(;nf<mframes;){
		sprite[nf++].unload();
		if(nf<mframes){
			sprite[nf-1].load(sprite[nf].getname());
		}
	}
	mframes--;
}

void csprite::finalize(){
	int i;
	for(i=0;i<mframes;i++){
		sprite[i].unload();
	}
	delete sprite;
}

int csprite::frames(char c){
	if(c='f')return frame;
	if(c='m')return mframes;
	if(c='n')return nframes;
	return -1;
}

void csprite::minframe(){
	selframe(frame-1);
}

void csprite::pframe(){
	selframe(frame+1);
}

void csprite::selframe(int nf){
	if(nf>=0&&nf<mframes){
		frame=nf;
	}
	else{
		frame=-1;
	}
}


void CSprite::addx(int x){
	posx+=x;
}

void CSprite::addy(int y){
	posy+=y;
}

int CSprite::distx(CSprite *Sprite){
	int dist;
	if((dist=(Sprite->posx-(posx+sprite->sprite[sprite->frame].img->w)))>0){
		return dist;
	}
	else if((dist=((Sprite->posx+Sprite->sprite->sprite[Sprite->sprite->frame].img->w)-posx))>0){
		return 0;
	}
	else{
		return dist;
	}
}

int CSprite::disty(CSprite *Sprite){
	int dist;
	if((dist=(Sprite->posy-(posy+sprite->sprite[sprite->frame].img->h)))>0){
		return dist;
	}
	else if((dist=((Sprite->posy+Sprite->sprite->sprite[Sprite->sprite->frame].img->h)-posy))>0){
		return 0;
	}
	else{
		return dist;
	}
}

void CSprite::draw(SDL_Surface *superficie){
	SDL_Rect dest;
	dest.x=posx;
	dest.y=posy;
	SDL_BlitSurface(sprite->sprite[frame].img,NULL,superficie,*dest);
}

int CSprite::geth(){
	return sprite->sprite[sprite->frame].img->h;
}

int CSprite::getw(){
	return sprite->sprite[sprite->frame].img->w;
}

int CSprite::getx(){
	return posx;
}

int CSprite::gety(){
	return posy;
}

void CSprite::load(csprite *Sprite){
	int i;
	sprite=new(sprite)(Sprite->nframes);
	for(i=0;i<Sprite->mframes;i++){
		sprite->addframe(Sprite->sprite[i]);
	}
	sprite->frame=Sprite->frame;
}

csprite *CSprite::read(){
	return sprite;
}

void CSprite::setx(int x){
	posx=x;
}

void CSprite::sety(int y){
	posy=y;
}

int CSprite::superposicion(CSprite *Sprite){
	if(distx(Sprite)==0&&disty(Sprite)==0){
		return 1;
	}
	return 0;
}