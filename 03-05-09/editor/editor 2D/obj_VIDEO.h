#ifndef obj_VIDEO_h
#define obj_VIDEO_h

#include <SDL.h>
#include "C_lista_enlazada.h"

namespace obj_SDL{

	namespace obj_VIDEO{

		//declaraciones

		class SUBPANTALLA;

		class PANTALLA{
		private:
			SDL_Surface *pantalla;
			C_lista_enlazada::nodo<SUBPANTALLA> *subpantallas;
			int w,h;
			bool activa;
		public:
			PANTALLA();
			~PANTALLA();
			void establecer(int W,int H);
			SDL_Rect *agregar(SDL_Surface *sub,SDL_Rect area);
			void mostrar();
			void ocultar();
#ifdef fun_IMG_h
			void PutPixel(int x,int y,Uint32 pixel){
				obj_SDL::PutPixel(pantalla,x,y,pixel);
			};
			void DrawLine(int x1,int y1,int x2,int y2,Uint32 pixel){
				obj_SDL::DrawLine(pantalla,x1,y1,x2,y2,pixel);
			};
#endif//fun_IMG_h
		};

		class SUBPANTALLA{
		private:
			friend PANTALLA;
			SDL_Surface *pantalla;
			SDL_Rect org,ORG,dest;
		public:
			SUBPANTALLA();
			~SUBPANTALLA();
		};

		//definiciones

		//PANTALLA
		PANTALLA::PANTALLA(){
			activa=0;
			h=0;
			w=0;
			pantalla=NULL;
			subpantallas=new C_lista_enlazada::nodo<SUBPANTALLA>(new SUBPANTALLA);
			SDL_Init(SDL_INIT_VIDEO);
		}

		PANTALLA::~PANTALLA(){
			if(subpantallas)delete subpantallas;
			SDL_Quit();
		}

		SDL_Rect *PANTALLA::agregar(SDL_Surface *sub,SDL_Rect area){
			SUBPANTALLA *aux=subpantallas->escrivir(new SUBPANTALLA);
			aux->pantalla=sub;
			aux->dest.x=area.x;
			aux->dest.y=area.y;
			aux->org.w=area.w;
			aux->org.h=area.h;
			aux->ORG.x=0;
			aux->ORG.y=0;
			return &aux->ORG;
		}

		void PANTALLA::establecer(int W,int H){
			if(H>=0)h=H;
			if(W>=0)w=W;
		}

		void PANTALLA::mostrar(){
			int i=1;
			SUBPANTALLA *aux,*aux2=NULL;
			//si esta desactivado lo activa
			if(!pantalla){
				SDL_Init(SDL_INIT_VIDEO);
				pantalla=SDL_SetVideoMode(w,h,32,SDL_HWSURFACE);
			}
			//compone la pantalla
			do{
				if(aux2==(aux=(*subpantallas)[i++]))i=-1;
				else{
					aux->org.x=aux->ORG.x;
					aux->org.y=aux->ORG.y;
					SDL_BlitSurface(aux->pantalla,&(aux->org),pantalla,&(aux->dest));
					aux2=aux;
				}
			}while(i>=0);
			//actualiza la pantalla
			SDL_Flip(pantalla);
		}

		void PANTALLA::ocultar(){
			SDL_Quit();
			pantalla=NULL;
		}

		//SUBPANTALLA
		SUBPANTALLA::SUBPANTALLA(){
			pantalla=NULL;
			ORG.x=0;
			ORG.y=0;
		}

		SUBPANTALLA::~SUBPANTALLA(){
			if(pantalla)delete pantalla;
		}
	}

	//declarar objetos particulares

	obj_VIDEO::PANTALLA pantalla;

}

#endif//obj_VIDEO_H