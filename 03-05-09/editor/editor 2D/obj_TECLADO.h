#ifndef obj_TECLADO_h
#define obj_TACLADO_h

#include <SDL.h>

namespace obj_SDL{

	namespace obj_TECLADO{

		//declaraciones

		class TECLADO{
		private:
			char *cad;
			char separador;
		public:
			TECLADO();
			~TECLADO();
			char *leer_cadena();
			int leer_int();
			char &final();
		};

		//definiciones

		TECLADO::TECLADO(){
			cad=NULL;
			separador=13;//'\n'==10 INTRO==13
		}

		TECLADO::~TECLADO(){
			if(cad)delete cad;
		}

		char *TECLADO::leer_cadena(){
			int i=0,j;
			char *CAD=NULL;
			SDL_Event evento;
			SDL_EnableUNICODE(1);
			do{
				if(SDL_PollEvent(&evento))if(evento.type==SDL_KEYDOWN){
					if(i){
						CAD=new char[i];
						for(j=0;j<i;j++)CAD[j]=cad[j];
						delete cad;
						cad=new char[++i];
						for(j=0;j<(i-1);j++)cad[j]=CAD[j];
						delete CAD;
					}
					else cad=new char[++i];
					cad[i-1]=evento.key.keysym.unicode&0x7f;
				}
			}while((!cad)||(cad[i-1]!=separador));
			cad[i-1]='\0';
			SDL_EnableUNICODE(0);
			return cad;
		}

		int TECLADO::leer_int(){
			int i=0;
			char c=0;
			SDL_Event evento;
			SDL_EnableUNICODE(1);
			do{
				if(SDL_PollEvent(&evento))if(evento.type==SDL_KEYDOWN){
					c=evento.key.keysym.unicode&0x7f;
					c-='0';
					if((c<10)&&(c>-1))i=(i*10)+(c);
					else c=-1;
				}
			}while(c!=-1);
			SDL_EnableUNICODE(0);
			return i;
		}

		char &TECLADO::final(){
			return separador;
		}

	}

	//declarar objetos particulares

	obj_TECLADO::TECLADO teclado;

}

#endif//obj_TECLADO_h