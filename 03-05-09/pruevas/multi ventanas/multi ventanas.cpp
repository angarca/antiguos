#include <stdio.h>
#include <conio.h>
#include <SDL.h>

int main(int argc,char *argv[]){

	SDL_Surface *pantalla,*img,*pan;
	SDL_Event evento;
	int done=1;
	Uint32 rmask=0xff000000,gmask=0x00ff0000,bmask=0x0000ff00,amask=0x000000ff;

	SDL_Init(SDL_INIT_VIDEO);
	pantalla=SDL_SetVideoMode(640,480,24,0);

	img=SDL_LoadBMP("cursor.bmp");

	pan=SDL_CreateRGBSurface(0,640,480,24,0,0,0,0);
	SDL_SetColorKey(pan,SDL_SRCCOLORKEY|SDL_RLEACCEL,SDL_MapRGB(pan->format,255,255,255));
	//pan=SDL_LoadBMP("parche.bmp");//parche

	SDL_BlitSurface(img,NULL,pan,NULL);
	SDL_Flip(pan);
	SDL_BlitSurface(pan,NULL,pantalla,NULL);
	SDL_Flip(pantalla);
	//SDL_UpdateRect(pantalla,0,0,10,10);

	while(done){SDL_PollEvent(&evento);if(evento.type==SDL_KEYDOWN)done=0;}

	return 0;

}