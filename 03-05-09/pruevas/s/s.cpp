// Listado: cliente.cpp
//
// Cliente TCP usando SDL_net

#include <stdio.h>

#include <iostream>
#include <SDL.h>
#include <SDL_net.h>

using namespace std;

 
int main(int argc, char **argv)
{
	FILE *debug=fopen("debug.txt","w");
	fputc('h',debug);
	fclose(debug);

    // Comprobamos los parámetros
	
    if (argc < 3) {
	
	cerr << "Uso: "<< argv[0] << " servidor puerto" << endl;
	exit(1);
	
    }
	debug=fopen("debug.txt","w");
	fputc('o',debug);
	fclose(debug);
 
    // Inicializamos SDL_net
	
    if (SDLNet_Init() < 0) {
	
	cerr << "SDLNet_Init(): " << SDLNet_GetError() << endl ;
	exit(1);
    }
	debug=fopen("debug.txt","w");
	fputc('l',debug);
	fclose(debug);

    // Resolvemos el host (servidor)

    IPaddress ip;
 
    if (SDLNet_ResolveHost(&ip, argv[1], atoi(argv[2])) < 0) {
	
	cerr << "SDLNet_ResolveHost(): "<< SDLNet_GetError() << endl;
	exit(1);

    }
	debug=fopen("debug.txt","w");
	fputc('a',debug);
	fclose(debug);

    // Abrimos una conexión con la IP provista
    
    TCPsocket socket;

    if (!(socket = SDLNet_TCP_Open(&ip))) {

	cerr << "SDLNet_TCP_Open: " << SDLNet_GetError() << endl;
	exit(1);
    }
	debug=fopen("debug.txt","w");
	fputc('m',debug);
	fclose(debug);
 
    // Enviamos los mensaje

    bool terminar = false;
    char buffer[512];
    int longitud;
	debug=fopen("debug.txt","w");
	fputc('u',debug);
	fclose(debug);
    
    while(terminar == false) {

	cout << "Escribe algo :> " ;
	cin >> buffer;
 
	// Lo escrito + terminador
	
	longitud = strlen(buffer) + 1;

	// Lo enviamos

	if (SDLNet_TCP_Send(socket, (void *)buffer, longitud) < longitud) {
	    
	    cerr << "SDLNet_TCP_Send: " << SDLNet_GetError() << endl;;
	    exit(1);
	}
	debug=fopen("debug.txt","w");
	fputc('n',debug);
	fclose(debug);

	// Si era salir o cerrar, terminamos con la conexión
 
	if(strcmp(buffer, "exit") == 0)
	    terminar = true;

	if(strcmp(buffer, "quit") == 0)
	    terminar = true;
    }
	debug=fopen("debug.txt","w");
	fputc('d',debug);
	fclose(debug);
    
    SDLNet_TCP_Close(socket);
    SDLNet_Quit();
	debug=fopen("debug.txt","w");
	fputc('o',debug);
	fclose(debug);
    
    return 0;
}
