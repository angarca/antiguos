#include <stdio.h>
#include <direct.h>

void main(){

	FILE *archivo,*dest;
	char c;
	archivo=fopen("salida/cifrado.txt","rb");
	dest=fopen("salida/final.txt","wb");
	do{
		c=0x00;
		c|=(fgetc(archivo))*0x10;
		c|=fgetc(archivo);
		if(c!=-1)fputc(c,dest);
	}while(c!=-1);
	fclose(dest);
	fclose(archivo);

}