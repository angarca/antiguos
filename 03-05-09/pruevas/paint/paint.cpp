#include <SDL.h>

int main(int argc,char *argv[]){

	SDL_Surface *p;
	int x,y,bpp;
	Uint32 pixel=0x007fffff;
	Uint8 *b;
	SDL_Event evento;

	SDL_Init(SDL_INIT_VIDEO);
	p=SDL_SetVideoMode(200,200,32,SDL_HWSURFACE);
	bpp=p->format->BytesPerPixel;

	if(!( SDL_LockSurface(p) < 0 )){

		for(x=0;x<200;x++)for(y=0;y<100;y++){
			b=(Uint8 *)p->pixels+y*p->pitch+x*bpp;

			switch (bpp){
			case 1:
				*b=(Uint8)pixel;
				break;
			case 2:
				*(Uint16 *)b=(Uint16)pixel;
				break;
			case 3:
		        if(SDL_BYTEORDER == SDL_BIG_ENDIAN) {
			        b[0] = ((Uint8)pixel >> 16) & 0xff;
			        b[1] = ((Uint8)pixel >> 8) & 0xff;
			        b[2] = (Uint8)pixel & 0xff;
			    } else {
			        b[0] = (Uint8)pixel & 0xff;
			        b[1] = ((Uint8)pixel >> 8) & 0xff;
			        b[2] = ((Uint8)pixel >> 16) & 0xff;
			    }
				break;
			case 4:
				*(Uint32 *)b=pixel;
				break;
			}
		}
		
		SDL_UnlockSurface(p);

		SDL_Flip(p);
	}

	while(!((SDL_PollEvent(&evento))&&(evento.type==SDL_KEYDOWN)));

	return 0;

}