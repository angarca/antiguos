#include <stdio.h>
#include <SDL.h>

int main(int argc,char *argv[]){
	int x,y,bpp;
	Uint32 pixel;
	Uint8 *p;
	SDL_Surface *img=SDL_LoadBMP("paint.bmp");
	bpp=img->format->BytesPerPixel;

	if(SDL_LockSurface(img)>=0){

		for(x=0;x<(img->w);x++)for(y=0;y<(img->h);y++){
			p=(Uint8 *)img->pixels+y*img->pitch+x*bpp;

			switch(bpp){
			case 1:
				pixel=*p;
				break;
			case 2:
				pixel=*(Uint16 *)p;
				break;
			case 3:
				if(SDL_BYTEORDER==SDL_BIG_ENDIAN){
					pixel=p[0] << 16 | p[1] << 8 | p[2];
				}else{
					pixel=p[0] | p[1] << 8 | p[2] << 16;
				}
				break;
			case 4:
				pixel=*(Uint32 *)p;
				break;
			}

			printf("\n%d\t%x",pixel,pixel);
		}

		SDL_UnlockSurface(img);
	}

	SDL_FreeSurface(img);

	return 0;
}