#ifndef cmapa_h
#define cmapa_h

namespace cmapa{

	class mapa{//el mapa completo
	private:
		int x,y,c,z,f;//variavles de entorno
		//x ancho del mapa
		//y alto del mapa
		//c n� de capas del mapa
		//z longitud de la paleta
		//f control general de fases sincronizadas
		int xpc,ypc;//ancho y alto de cada casilla
		capa *capas;//raiz de la lista de capas
		struct paleta{//structura para lista de sprites
			sprite *img;//el sprite
			paleta *sig;//siguien en la lista
			paleta(sprite *IMG);//sig=NULL;img=IMG;
		}*raiz;//raiz de la paleta
	public:
		mapa(int X,int Y);//pone todo a cer salvo 'x' e 'y' a las que asigna los parametros
		int addcapa(bool F);//a�ade capa en blanco al final de la lista, si F fase general predeterminada
		int operator()(int x,int y,int val,int fas);//asigna valor y fase a una casilla (si fase = -1 canvia el valor de fase general)
		int operator()(int x,int y);//retorna el flag de la casilla (por valor)
		int guardar(char *fname);
		int cargar();carga la paleta a partir de "paleta.txt"
	};

	class capa{//cada capa del mapa
	private:
		friend mapa;
		casilla **tabla;//tabla de casillas
		capa *sig;//siguiente capa de la lista
	public:
		capa(int x,int y);//crea tabla del tama�o especificado por los parametros y nulifica sig
		capa(int x,int y,int F);//"" pero con fase general activada en cada casilla
	};

	class casilla{//cada casilla de las tablas
	private:
		friend capa;
		friend mapa;
		int flag;//contiene informacion en forma de flag
		//0x XXXXX3ff->0-1023 sprite que ocupa la casilla segun el orden de la paleta
		//0B XXXXXXXXXXXXXXXXXXXXXX1111111111
		//0x XXX3fcXX->0-15	 la fase en caso de fase independiente
		//0B XXXXXXXXXXXXXX11111111XXXXXXXXXX
		//0x 8XXXXXXX->0-1	 indica fase independiente = 0   fase general = 1
		//0B 1XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
	public:
		casilla();//pone flag a 0x00000000
	};

}

#endif//cmapa_h