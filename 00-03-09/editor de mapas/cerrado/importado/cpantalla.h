#ifndef cpantalla_h
#define cpantalla_h

namespace cpantalla{

	class pantalla{//para manejar la pantalla (unica clase que inicia SDL)
	private:
		SDL_Surface *screen;//la imagen que se mostrara
	public:
		pantalla(int x,int y);//constructor declara screen inicializando SDL con el tama�o de los parametros
		~pantalla();//libera screen y cierra SDL
		int operator()(SDL_Surface *img,int x,int y);//copia una img en screen en las coordenadas de los parametros
		int draw();//actualiza screen en la verdadera pantalla
	};

}

#endif//cpantalla_h