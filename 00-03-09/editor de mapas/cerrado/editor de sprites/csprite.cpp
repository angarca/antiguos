#include <SDL.h>
#include <stdio.h>
#include "csprite.h"

namespace csprite{

	//funiones de frame

	frame::frame(char *name){
		img=SDL_LoadBMP(name);
		sprintf(nombre,name);
		sig=NULL;
	}

	frame::frame(frame *org){
		img=SDL_CreateRGBSurface(SDL_HWSURFACE,org->img->w,org->img->h,org->img->format->BitsPerPixel,0xff000000,0x00ff0000,0x0000ff00,0x000000ff);
		SDL_BlitSurface(org->img,NULL,img,NULL);
		sprintf(nombre,org->nombre);
		sig=NULL;
	}

	frame::~frame(){
		if(sig)delete sig;
		if(img)SDL_FreeSurface(img);
	}

	int sprite::guardar(char *fname){
		FILE *archivo;
		frame *aux;
		if(raiz){
			aux=raiz;
			archivo=fopen(fname,"wb");
			while(aux->sig){
				for(int i=0;aux->nombre[i]!='\0';i++)fputc(aux->nombre[i],archivo);
				fputc('\x7',archivo);
				aux=aux->sig;
			}
			for(int i=0;aux->nombre[i]!='\0';i++)fputc(aux->nombre[i],archivo);
			fputc('\0',archivo);
			fclose(archivo);
			return 0;
		}
		return -1;
	}

	int sprite::cargar(char *fname){
		FILE *archivo=NULL;
		char nombre[30];
		int i,done=0;
		if(archivo=fopen(fname,"rb")){
			while(!done){
				for(i=0;((nombre[i]=fgetc(archivo))!='\x7')&&!done;i++)if(nombre[i]=='\0')done=1;nombre[i]='\0';
				addframe(nombre);
			}
			fclose(archivo);
			return 0;
		}
		return -1;
	}

	//funciones de sprite

	sprite::sprite(){
		raiz=NULL;
	}

	int sprite::addframe(char *name){
		frame *aux;
		if(raiz){
			aux=raiz;
			while(aux->sig)aux=aux->sig;
			aux->sig=new frame(name);
			if(aux->sig->img)return 0;
			else delete aux->sig;
			aux->sig=NULL;
		}
		else{
			raiz=new frame(name);
			if(raiz->img)return 0;
			else delete raiz;
			raiz=NULL;
		}
		return -1;
	}

	int sprite::addframe(frame *org){
		frame *aux;
		if(raiz){
			aux=raiz;
			while(aux->sig)aux=aux->sig;
			aux->sig=new frame(org);
			if(aux->sig->img)return 0;
			else delete aux->sig;
			aux->sig=NULL;
		}
		else{
			raiz=new frame(org);
			if(raiz->img)return 0;
			else delete raiz;
			raiz=NULL;
		}
		return -1;
	}

	sprite::~sprite(){
		if(raiz)delete raiz;
	}

}