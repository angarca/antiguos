#ifndef csprite_h
#define csprite_h

#include <SDL.h>

namespace csprite{

	class frame;

	class sprite{//lista de frames
	private:
		frame *raiz;//raiz de la lista
	public:
		sprite();//constructor (nulifica raiz)
		~sprite();//vacia punteros
		int addframe(char *name);//carga un frame desde un archivo
		int addframe(frame *org);//a�ade uu frame como copia de original
		int guardar(char *fname);
		int cargar(char *fname);
	};

	class frame{//cada frame de un sprite
	private:
		friend sprite;
		SDL_Surface *img;//la imagen
		frame *sig;//el siguiente de la lista
		char nombre[30];
	public:
		frame(char *name);//carga el fichero name como img
		frame(frame *org);//lo crea como copia del orginal
		~frame();//vacia punteros
	};

}

#endif//csprite_h