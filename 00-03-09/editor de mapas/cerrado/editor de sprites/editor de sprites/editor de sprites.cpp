#include <stdio.h>
#include <conio.h>

#include "csprite.h"

int main(int argc,char *argv[]){

	int salida=0,done[2];
	char c,cad[30];
	csprite::sprite *Sprite;

	while(!salida){
		Sprite=NULL;
		Sprite=new csprite::sprite;
		printf("\nquieres crear o abrir un sprite (c/a)? : ");
		c=getche();
		if(c=='a'){
			done[0]=0;
			while(!done[0]){
				printf("\nel nombre por favor : ");
				scanf("%s%c",&cad,&c);
				if(Sprite->cargar(cad)==-1)printf("\nno se ha podido cargar el archivo.");
				else done[0]=1;
			}
		}
		done[0]=0;
		while(!done[0]){
			printf("\nquieres agregar un frame al sprite (s/n)? : ");
			c=getche();
			if(c=='s'){
				done[1]=0;
				while(!done[1]){
					printf("\nel nombre por favor : ");
					scanf("%s%c",&cad,&c);
					if(Sprite->addframe(cad)==-1)printf("\nno se ha podido cargar el archivo.");
					else done[1]=1;
				}
			}
			else done[0]=1;
		}
		printf("\nquieres guardar los cambios en este sprite (s/n)? : ");
		c=getche();
		if(c=='s'){
			done[0]=0;
			while(!done[0]){
				printf("\nel nombre por favor : ");
				scanf("%s%c",&cad,&c);
				if(Sprite->guardar(cad)==-1)printf("\nno se ha podido crear el archivo");
				else done[0]=1;
			}
		}
		printf("\nquieres salir del editor (s/n)? : ");
		c=getche();
		if(c=='s') salida=1;
		delete Sprite;
	}

	return 0;

}