#include <stdio.h>
#include <conio.h>
#include <SDL.h>

int main(int argc,char *argv[]){

	SDL_Surface *screen;
	int done=0;
	//char buf[100];
	//
	SDL_Rect dest;
	SDL_Surface *img=SDL_LoadBMP("capa.bmp");
	Uint8 *keys;
	dest.x=0;
	dest.y=0;

	SDL_Init(SDL_INIT_VIDEO);
	screen=SDL_SetVideoMode(640,640,24,SDL_HWSURFACE);

	while(!done){

		SDL_Flip(screen);

		//buf[0]=getch();
		keys=SDL_GetKeyState(NULL);

		if(keys[SDLK_c])done=1;
		if(keys[SDLK_a])SDL_BlitSurface(img,NULL,screen,&dest);
	}

	return 0;

}