#ifndef cpantalla_h
#define cpantalla_h

namespace cpantalla{

	class pantalla{//para manejar la pantalla (unica clase que inicia SDL)
	private:
		SDL_Surface *screen;//la imagen que se mostrara
	public:
		pantalla(int x,int y);//constructor screen=Screen;
		~pantalla();//cierra SDL
		int operator()(SDL_Surface *img,int x,int y);//copia una img en screen en las coordenadas de los parametros
		int draw();//actualiza screen en la verdadera pantalla
	};

}

#endif//cpantalla_h