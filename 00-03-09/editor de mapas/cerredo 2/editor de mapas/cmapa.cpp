#include <SDL.h>
#include "csprite.h"
#include "cmapa.h"

namespace cmapa{

	int trans(char *num);
	int trans(char *num){
		int i,j=1,n=0;
		for(i=0;num[i]!='\0';i++);
		while(i>0){
			n+=num[--i]*j;
			j*=10;
		}
		return n;
	}

	mapa::mapa(int X,int Y){
		c=0;
		capas=NULL;
		f=0;
		raiz=NULL;
		x=X;
		xpc=0;
		y=Y;
		ypc=0;
	}

	int mapa::CargarPaleta(){

		FILE *archivo;
		char c,buf[30],sbuf[100][20][30];
		int i=0,j=0,k=0,c=0;
		paleta *aux;
		archivo=fopen("paleta.txt","r");
		while(c<5){
			i=0;
			while((buf[i]=fgetc(archivo))!=',')i++;
			switch(c){
			case 0:
				xpc=trans(buf);
				break;
			case 1:
				ypc=trans(buf);
				break;
			case 2:
				/**/
				break;
			case 3:
				/**/
				break;
			case 4:
				/**/
				break;
			}
			c++;
		}
		i=0;
		c=0;
		while(!c){
			sbuf[i][j][k]=fgetc(archivo);
			if(sbuf[i][j][k]=='\"'){
				sbuf[i][j][k]='\0';
				k=0;
				j++;
			}
			if(sbuf[i][j][k]=='/'){
				sbuf[i][j][k]='\0';
				k=0;
				j=0;
				i++;
			}
			if(sbuf[i][j][k]=='_'){
				sbuf[i][j][k]='\0';
				c=1;
			}
			k++;
		}
		for(i=0;sbuf[i][0][0]!='\0';i++){
			if(raiz){
				aux=raiz;
				while(aux->sig)aux=aux->sig;
				aux->sig=new paleta;
				aux=aux->sig;
			}
			else{
				raiz=new paleta;
				aux=raiz
			}
			for(j=0;sbuf[i][j][0]!='\0';j++){
				aux->img->addframe(sbuf[i][j]);
			}
		}
		return 0;
	}

	int mapa::addcapa(bool F){
		capa *aux;
		if(capas){
			aux=capas;
			while(aux->sig)aux=aux->sig;
			aux->sig=new capa(x,y,F);
		}
		else{
			capas=new capa(x,y,F);
		}
	}

	int mapa::operator()(int C,int X,int Y){

		capa *aux;
		aux=capas;
		while(((C--)>0)&&(aux->sig))aux=aux->sig;
		return aux->tabla[X][Y].flag;
	}

	int mapa::operator()(int C,int X,int Y,int val,int fas){
		capa *aux;
		int *a;
		aux=capas;
		while(((C--)>0)&&(aux->sig))aux=aux->sig;
		a=&(aux->tabla[X][Y].flag);
		(*a)&=0xfffc0000;
		(*a)|=val&0x000003ff;
		(*a)|=(fas*1024)&0x0003fc00;
	}

}