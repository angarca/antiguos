#include <SDL.h>
#include "csprite.h"

namespace csprite{

	//funciones de frame

	frame::frame(char *name){

		img=SDL_LoadBMP(name);
		sig=NULL;

	}

	frame::~frame(){

		if(img)SDL_FreeSurface(img);
		if(sig)delete sig;

	}

	//funciones de sprite

	sprite::sprite(){

		raiz=NULL;

	}

	int sprite::addframe(char *name){

		frame *aux;

		if(raiz){

			aux=raiz;
			while(aux->sig)aux=aux->sig;
			aux->sig=new frame(name);

		}
		else{

			raiz=new frame(name);

		}

		return 0;

	}

	sprite::~sprite(){

		delete raiz;

	}

}