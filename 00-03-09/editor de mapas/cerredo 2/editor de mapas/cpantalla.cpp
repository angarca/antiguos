#include <SDL.h>
#include "cpantalla.h"

namespace cpantalla{

	pantalla::pantalla(int x,int y){
		SDL_Init(SDL_INIT_VIDEO);
		screen=SDL_SetVideoMode(x,y,24,SDL_HWSURFACE);
	}

	pantalla::~pantalla(){
		SDL_Quit();
	}

	int pantalla::draw(){
		SDL_Flip(screen);
		return 0;
	}

	int pantalla::operator()(SDL_Surface *img,int x,int y){
		SDL_Rect dest;
		dest.x=x;
		dest.y=y;
		SDL_BlitSurface(img,NULL,screen,&dest);
		return 0;
	}

}