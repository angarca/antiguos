#include <cstdio>
#include <SDL.h>
#include "csprite.h"
#include "cmapa.h"
#include "cpantalla.h"

int/**/ *SeleccionDeMapa();
void mostrar(int/**/ *MAPA);
bool esperar(int/**/ *MAPA);
bool guardar(int/**/ *MAPA);

int main(int argc,char *argv[]){

	int salidas=0xffffffff;
	int/**/ *MAPA;
	cpantalla::pantalla *screen;

	while(salidas&0x80000000){

		MAPA=SeleccionDeMapa();

		while(salidas&0x40000000){

			
			screen=new cpantalla::pantalla(10/**/,10/**/);

			mostrar(MAPA);

			if(esperar(MAPA))salidas&=0xbfffffff;

			delete screen;

		}

		salidas|=0x40000000;

		if(guardar(MAPA))salidas&=0x7fffffff;

	}

	delete MAPA;

	return 0;

}

int/**/ *SeleccionDeMapa(){
	int *a=new int;
	printf("\nSeleccionDeMapa");
	return a;
}

void mostrar(int/**/ *MAPA){
	printf("\nmostrar");
}

bool esperar(int/**/ *MAPA){
	char c;
	printf("\nesperar\n");
	scanf("%c%c",&c,&c);
	if(c!='a')return 0;
	return 1;
}

bool guardar(int/**/ *MAPA){
	char c;
	printf("\nguardar\n");
	scanf("%c%c",&c,&c);
	if(c!='b')return 0;
	return 1;
}