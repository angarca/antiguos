#ifndef csprite_h
#define csprite_h

namespace csprite{

	class frame;

	class sprite{//lista de frames
	private:
		frame *raiz;//raiz de la lista
	public:
		sprite();//constructor (nulifica raiz)
		~sprite();//vacia punteros
		int addframe(char *name);//carga un frame desde un archivo
	};

	class frame{//cada frame de un sprite
	private:
		friend sprite;
		SDL_Surface *img;//la imagen
		frame *sig;//el siguiente de la lista
	public:
		frame(char *name);//carga el fichero name como img
		~frame();//vacia punteros
	};

}

#endif//csprite_h