#ifndef c_cadena
#define c_cadena
class cadena
{
private:
	char *cad;
	int longitud;
	void nueva(int l);
public:
	cadena(){nueva(0);};
	cadena(cadena &cad2){nueva(0);*this=cad2;};
	cadena(char *cad2){nueva(0);*this=cad2;};
	~cadena(){delete(cad);};
	char &operator[](int i);
	cadena &operator=(char *cad2);
	cadena &operator=(cadena &cad2);
	bool operator==(cadena &cad2);
	bool operator==(char *cad2);
	cadena &operator<<(cadena &cad2);
	cadena &operator<<(char *cad2);
	cadena &operator<<(char c);
	int operator>>(char c);
	cadena operator>>(int n);
	cadena operator+(cadena &cad2);
	cadena operator+(char *cad2);
	cadena operator+(char c);
	int operator-(char c);
	cadena operator-(int n);
	cadena &operator--();
	cadena *operator/(char c);
	int operator()();
	int operator()(char c);
};


void cadena::nueva(int l)
{
	longitud=l;
	cad=new(char [l]);
	if(l==1)cad[0]='\0';
}


char &cadena::operator[](int i)
{
	return(cad[i]);
}

cadena &cadena::operator=(cadena &cad2)
{
	int i;
	longitud=cad2.longitud;
	delete(cad);
	nueva(longitud);
	for(i=0;i<longitud;i++)cad[i]=cad2[i];
	return(*this);
}

cadena &cadena::operator=(char *cad2)
{
	int i;
	for(i=0;cad2[i]!='\0';i++);
	delete(cad);
	nueva(i);
	for(i=0;i<longitud;i++)cad[i]=cad2[i];
	return(*this);
}

bool cadena::operator ==(cadena &cad2)
{
	int i;
	if(longitud!=cad2.longitud)return(0);
	for(i=0;i<longitud;i++)if(cad[i]!=cad2[i])return(0);
	return(1);
}

bool cadena::operator==(char *cad2)
{
	cadena cad3;
	return(*this==(cad3=cad2));
}

cadena &cadena::operator <<(cadena &cad2)
{
	int i=longitud,j;
	char *cad3;
	cad3=cad;
	nueva((longitud+cad2.longitud));
	for(j=0;j<i;j++)cad[j]=cad3[j];
	delete(cad3);
	for(;i<longitud;i++)cad[i]=cad2[i-j];
	return(*this);
}

cadena &cadena::operator <<(char *cad2)
{
	cadena cad3;
	return(*this<<(cad3=cad2));
}

cadena &cadena::operator <<(char c)
{
	char *cad2;
	cad2=new(char [2]);
	cad2[0]=c;
	cad2[1]='\0';
	(*this)<<cad2;
	delete(cad2);
	return(*this);
}

int cadena::operator>>(char c)
{
	int i;
	for(i=0;i<longitud&&cad[i]!=c;i++);
	return(i);
}

cadena cadena::operator >>(int n)
{
	cadena cad2;
	char *cad3;
	int i,j;
	cad3=new(char [((j=longitud-n)+1)]);
	for(i=0;i<j;i++)cad3[i]=cad[i+n];
	cad3[i]='\0';
	cad2=cad3;
	delete(cad3);
	cad3=cad;
	nueva(n);
	for(i=0;i<longitud;i++)cad[i]=cad3[i];
	delete(cad3);
	return(cad2);
}


cadena cadena::operator +(cadena &cad2)
{
	cadena cad3;
	cad3=*this;
	return(cad3<<cad2);
}

cadena cadena::operator +(char *cad2)
{
	cadena cad3;
	return(*this+(cad3=cad2));
}

cadena cadena::operator +(char c)
{
	cadena cad3;
	cad3=*this;
	return(cad3<<c);
}

int cadena::operator -(char c)
{
	int n=((*this)>>c);
	if(n==(*this)())n=0;
	else n++;
	return(n);
}

cadena cadena::operator -(int n)
{
	cadena cad3;
	cad3=*this;
	return(cad3>>n);
}

cadena &cadena::operator --()
{
	if(longitud==0)return(*this);
	cadena cad2;
	cad2=*this;
	cad2.longitud--;
	(*this)=cad2;
	return(*this);
}

cadena *cadena::operator /(char c)
{
	int i,j;
	cadena *pcad=new(cadena [1]),*p;
	for(i=0;((*this)-c)!=0;i++)
	{
		pcad[i]=(*this);
		(*this)=pcad[i]>>(pcad[i]-c);
		--pcad[i];
		p=pcad;
		pcad=new(cadena [i+2]);
		for(j=0;j<i+1;j++)pcad[j]=p[j];
	};
	pcad[i]=(*this);
	return(pcad);
}

int cadena::operator ()()
{
	return(longitud);
}

int cadena::operator ()(char c)
{
	int i,n=0;
	cadena cad2,cad3(*this);
	for(i=0;i<longitud;i++)
	{
		if(cad[i]==c)
		{

			cad2=(cad3>>((cad3>>c)+1));
			--cad3;
			cad3<<cad2;
			n++;
		};
	};
	(*this)=cad3;
	return(n);
}
#endif