#include <time.h>
#include <stdlib.h>
#include <conio.h>
#include <stdio.h>
class pantalla
{
private:
	char *lineas[24];
	char vacio[2],saludo[18],peticion[38];
	char lleva[12],mul1[14],mul2[14],raya[14],res[14],correc[14];
	char val[22],otra[20];
	char valp[6],valn[22],otraa[20];
	char posicion;
	int mu1,mu2,re;
	void fmul1();
	void fmul2();
	void fcorrec();
	int aleatoria(int n);
	void unir(int i);
	void comprovar();
	int el(int n,int m);
	void cop(char *rec,const char *emi);
public:
	pantalla()
	{
		vacio[0]='\n';
		vacio[1]='\0';
		cop(saludo,"buenos dias jose");
		cop(peticion,"vamos ha hacer unas multiplicaciones");
		lleva[0]='\0';
		mul1[0]='\0';
		mul2[0]='\0';
		raya[0]='\0';
		res[0]='\0';
		correc[0]='\0';
		val[0]='\0';
		otra[0]='\0';
		cop(valp,"bien");
		cop(valn,"mal lo correcto es: ");
		cop(otraa,"quieres hacer otra");
		seguir=1;
		fin=1;
		completa[0]='\0';
	};
	void unirt();
	void entradas();
	void construir();
	void blanquear();
	bool seguir,fin;
	char completa[220];
};


void main()
{
	int i;
	pantalla una;
	una.construir();
	do
	{
		una.blanquear();
		do
		{
			una.unirt();
			for(i=0;una.completa[i]!='\0';i++)printf("%c",una.completa[i]);
			una.entradas();
		}while(una.fin);
	}while(una.seguir);
}


void pantalla::construir()
{
	int i;
	saludo[16]='\n';
	saludo[17]='\0';
	peticion[36]='\n';
	peticion[37]='\0';
	raya[0]='\t';
	for(i=1;i<12;i++)raya[i]='_';
	raya[12]='\n';
	raya[13]='\0';
	valp[4]='\n';
	valp[5]='\0';
	valn[20]='\n';
	valn[21]='\0';
	otraa[18]='\n';
	otraa[19]='\0';
	lineas[0]=vacio;
	lineas[1]=vacio;
	lineas[2]=saludo;
	lineas[3]=peticion;
	lineas[4]=vacio;
	lineas[5]=lleva;
	lineas[6]=mul1;
	lineas[7]=mul2;
	lineas[8]=raya;
	lineas[9]=res;
	lineas[10]=correc;
	lineas[11]=vacio;
	lineas[12]=val;
	lineas[13]=vacio;
	lineas[14]=otra;
	for(i=15;i<24;i++)lineas[i]=vacio;
}


void pantalla::blanquear()
{
	int i;
	lleva[0]='\t';
	for(i=1;i<10;i++)lleva[i]=' ';
	lleva[10]='\n';
	lleva[11]='\0';
	res[0]='\t';
	for(i=1;i<11;i++)res[i]=' ';
	res[11]='_';
	res[12]='\n';
	res[13]='\0';
	cop(correc,vacio);
	cop(val,vacio);
	cop(otra,vacio);
	fmul1();
	fmul2();
	posicion='u';
	fin=1;
}


void pantalla::fmul1()
{
	int n,i;
	n=aleatoria(99999);
	mu1=n;
	mul1[0]='\t';
	mul1[1]=' ';
	mul1[2]=' ';
	mul1[13]='\0';
	mul1[12]='\n';
	for(i=11;i>2;i-=2)
	{
		mul1[i]=((n%10)+'0');
		n-=(n%10);
		n/=10;
	};
	for(i=4;i<12;i+=2)mul1[i]=' ';
}


void pantalla::fmul2()
{
	int n,i;
	n=aleatoria(9);
	mu2=n;
	mul2[0]='\t';
	mul2[1]=' ';
	mul2[2]=' ';
	mul2[3]='x';
	for(i=4;i<11;i++)mul2[i]=' ';
	mul2[11]='0'+n;
	mul2[12]='\n';
	mul2[13]='\0';
}


int pantalla::aleatoria(int n)
{
	int aleatorio;
	srand(time(NULL));
	aleatorio=rand();
	aleatorio=(aleatorio%n)+1;
	return (aleatorio);
}


void pantalla::unirt()
{
	int i;
	completa[0]='\0';
	for(i=0;i<24;i++)unir(i);
}


void pantalla::unir(int i)
{
	int j,k;
	for(j=0;completa[j]!='\0';j++);
	for(k=0;lineas[i][k]!='\0';k++)
	{
		completa[j]=lineas[i][k];
		j++;
	};
	completa[j]='\0';
}


void pantalla::entradas()
{
	char in;
	int i;
	in=getch();
	if(posicion=='u')
	{
		if(in>='0'&&in<='9')
		{
			res[11]=in;
			posicion='d';
			res[9]='_';
		}
		else if(in=='\x9')
		{
			for(i=11;i>0;i-=2)res[i]='0';
			comprovar();
		};
	}
	else if(posicion=='d')
	{
		if(in>='0'&&in<='9')
		{
			res[9]=in;
			posicion='c';
			res[7]='_';
		}
		else if(in=='\x8')
		{
			res[9]=' ';
			posicion='u';
			res[11]='_';
		}
		else if(in=='l')
		{
			res[9]=' ';
			posicion='D';
			lleva[9]='_';
		}
		else if(in=='\x9')
		{
			for(i=9;i>0;i-=2)res[i]='0';
			comprovar();
		};
	}
	else if(posicion=='c')
	{
		if(in>='0'&&in<='9')
		{
			res[7]=in;
			posicion='m';
			res[5]='_';
		}
		else if(in=='\x8')
		{
			res[7]=' ';
			posicion='d';
			res[9]='_';
		}
		else if(in=='l')
		{
			res[7]=' ';
			posicion='C';
			lleva[7]='_';
		}
		else if(in=='\x9')
		{
			for(i=7;i>0;i-=2)res[i]='0';
			comprovar();
		};
	}
	else if(posicion=='m')
	{
		if(in>='0'&&in<='9')
		{
			res[5]=in;
			posicion='s';
			res[3]='_';
		}
		else if(in=='\x8')
		{
			res[5]=' ';
			posicion='c';
			res[7]='_';
		}
		else if(in=='l')
		{
			res[5]=' ';
			posicion='M';
			lleva[5]='_';
		}
		else if(in=='\x9')
		{
			for(i=5;i>0;i-=2)res[i]='0';
			comprovar();
		};
	}
	else if(posicion=='s')
	{
		if(in>='0'&&in<='9')
		{
			res[3]=in;
			posicion='x';
			res[1]='_';
		}
		else if(in=='\x8')
		{
			res[3]=' ';
			posicion='m';
			res[5]='_';
		}
		else if(in=='l')
		{
			res[3]=' ';
			posicion='S';
			lleva[3]='_';
		}
		else if(in=='\x9')
		{
			for(i=3;i>0;i-=2)res[i]='0';
			comprovar();
		};
	}
	else if(posicion=='x')
	{
		if(in>='0'&&in<='9')
		{
			res[1]=in;
			comprovar();
		}
		else if(in=='\x8')
		{
			res[1]=' ';
			posicion='s';
			res[3]='_';
		}
		else if(in=='l')
		{
			res[1]=' ';
			posicion='X';
			lleva[1]='_';
		}
		else if(in=='\x9')
		{
			res[1]='0';
			comprovar();
		};
	}
	else if(posicion=='D')
	{
		if(in>='0'&&in<='9')
		{
			lleva[9]=in;
			posicion='d';
			res[9]='_';
		}
		else if(in=='l')
		{
			lleva[9]=' ';
			posicion='d';
			res[9]='_';
		};
	}
	else if(posicion=='C')
	{
		if(in>='0'&&in<='9')
		{
			lleva[7]=in;
			posicion='c';
			res[7]='_';
		}
		else if(in=='l')
		{
			lleva[7]=' ';
			posicion='c';
			res[7]='_';
		};
	}
	else if(posicion=='M')
	{
		if(in>='0'&&in<='9')
		{
			lleva[5]=in;
			posicion='m';
			res[5]='_';
		}
		else if(in=='l')
		{
			lleva[5]=' ';
			posicion='m';
			res[5]='_';
		};
	}
	else if(posicion=='S')
	{
		if(in>='0'&&in<='9')
		{
			lleva[3]=in;
			posicion='s';
			res[3]='_';
		}
		else if(in=='l')
		{
			lleva[3]=' ';
			posicion='s';
			res[3]='_';
		};
	}
	else if(posicion=='X')
	{
		if(in>='0'&&in<='9')
		{
			lleva[1]=in;
			posicion='x';
			res[1]='_';
		}
		else if(in=='l')
		{
			lleva[1]=' ';
			posicion='x';
			res[1]='_';
		};
	}
	else if(posicion=='n')
	{
		fin=0;
		if(in!='s')seguir=0;
	};
}


void pantalla::comprovar()
{
	int i,n=0;
	posicion='n';
	for(i=0;i<6;i++)
	{
		n+=((res[11-(i*2)])-'0')*el(10,i);
	};
	if(n==mu1*mu2)cop(val,valp);
	else 
	{
		cop(val,valn);
		fcorrec();
	};
	cop(otra,otraa);
}



void pantalla::fcorrec()
{
	int n,i;
	n=mu1*mu2;
	correc[0]='\t';
	correc[13]='\0';
	correc[12]='\n';
	for(i=11;i>0;i-=2)
	{
		correc[i]=((n%10)+'0');
		n-=(n%10);
		n/=10;
	};
	for(i=2;i<12;i+=2)correc[i]=' ';
}


int pantalla::el(int n,int m)
{
	int i;
	for(i=1;m>0;m--)i*=n;
	return(i);
}


void pantalla::cop(char *rec,const char *emi)
{
	int i,j=0;
	for(i=0;emi[i]!='\0';i++)
	{
		rec[j]=emi[i];
		j++;
	};
	rec[j]='\0';
}









