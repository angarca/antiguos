#ifndef c_B
#define c_B
class B
{
private:
	bool v;
	B *Iz,*De;
public:
	B(){v=0;Iz=0;De=0;};
	B(B *o,bool p){v=0;if(p){Iz=o;De=0;}else{Iz=0;De=o;};};
	bool &operator[](){return (v);};
	B *operator[](bool d){if(d){return (De);}else{return (Iz);};
};
#endif