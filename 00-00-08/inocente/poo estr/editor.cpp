#include <stdio.h>
#include <conio.h>
#include <windows.h>
struct memoria
{
	char caracter;
	bool sonido;
	char pausa;
	bool retraso;
};
struct coordenada
{
	char x;
	char y;
};
class pantalla
{
private:
	memoria texto[25][80];
	coordenada cursor;
public:
	pantalla()
	{
		for(int i=0;i<25;i++)for(int j=0;j<80;j++){texto[i][j].caracter=0;texto[i][j].sonido=0;texto[i][j].pausa=0;texto[i][j].retraso=0;cursor.x=0;cursor.y=0;};
	};
	memoria *operator[](int n);
	char operator[](char c);
	void mover(int x,int y);
	void cls(bool a);
	void operator()(char c);
	void borrar();
	void desplazar(char d);
	void efectos(coordenada *cur);
};


void main()
{
	FILE *archivo;
	int i;
	do
	{
		pantalla pan;
		char nombre[30];
		char c;
		printf("quieres crear o abrir un mensaje a/c\n");
		if((c=getch())=='a')
		{
			printf("el nombre:\n");
			for(i=0;i<27&&((nombre[i]=getche())!='\xd');i++)if(nombre[i]=='x8')i-=2;printf("\n");
			nombre[i++]='.';nombre[i++]='c';nombre[i]='\0';
			archivo=fopen(nombre,"r+b");
			for(i=0;i<25;i++)for(int j=0;j<80;j++)
			{
				pan[i][j].pausa=fgetc(archivo);
				pan[i][j].retraso=(bool)fgetc(archivo);
				pan[i][j].sonido=(bool)fgetc(archivo);
				pan[i][j].caracter=fgetc(archivo);
			};
			rewind(archivo);
		}
		else if(c=='c')
		{
			printf("el nombre:\n");
			for(i=0;i<27&&(nombre[i]=getche())!='\xd';i++)if(nombre[i]=='\x8')i-=2;printf("\n");
			nombre[i++]='.';nombre[i++]='c';nombre[i]='\0';
			archivo=fopen(nombre,"wb");
		}
		else printf("esa no es una opcion");
		if(archivo)
		{
			pan.cls(1);
			bool s=1;
			do
			{
				coordenada *cur=new(coordenada);
				char c;
				if((c=getch())=='\xd'){cur->x=pan['x'];cur->y=pan['y'];pan.cls(0);pan.efectos(cur);pan.cls(1);}
				else if(c=='\x8')pan.borrar();
				else if(c==-32)pan.desplazar(getch());
				else if(c=='\x9')s=0;
				else pan(c);
			}while(s);
			for(i=0;i<25;i++)for(int j=0;j<80;j++)
			{
				fputc(pan[i][j].pausa,archivo);
				fputc(pan[i][j].retraso,archivo);
				fputc(pan[i][j].sonido,archivo);
				fputc(pan[i][j].caracter,archivo);
			};
			fclose(archivo);
			pan.cls(0);
		};
		printf("deseas crear o abrir otro mensaje s/n\n");
	}while(getch()!='n');
}

void pantalla::efectos(coordenada *cur)
{;
	do{
		char a;
		char c;
		printf("deseas a�adir, quitar o conocer un efecto a/q/c\n");
		if((a=getch())=='a')
		{
			printf("que efecto deseas a�adir sonido/pausa/retraso s/p/r\n");
			if((c=getch())=='s')texto[cur->y][cur->x].sonido=1;
			else if(c=='p'){printf("de cuanto tiempo (s)\n");scanf("%d",&(texto[cur->y][cur->x].pausa));}
			else if(c=='r')texto[cur->y][cur->x].retraso=1;
			else printf("no has elegido un efecto valido\n");
		}
		else if(a=='q')
		{
			printf("que efecto deseas quitar sonido/pausa/retraso s/p/r\n");
			if((c=getch())=='s')texto[cur->y][cur->x].sonido=0;
			else if(c=='p')texto[cur->y][cur->x].pausa=0;
			else if(c=='r')texto[cur->y][cur->x].retraso=0;
			else printf("no has elegido un efecto valido\n");
		}
		else if(a=='c')
		{
			if(texto[cur->y][cur->x].sonido)printf("tiene sonido\n");
			if(texto[cur->y][cur->x].pausa)printf("tiene una pausa de %d segundos\n",texto[cur->y][cur->x].pausa);
			if(texto[cur->y][cur->x].retraso)printf("tiene un punto de inicio o finaizacion de retraso\n");
		}
		else printf("esa no es una opcion\n");
		printf("deseas a�adir, quitar o conocer otro efecto s/n\n");
	}while(getch()!='n');
}


memoria *pantalla::operator[](int n)
{
	return(texto[n]);
}

char pantalla::operator[](char c)
{
	if(c=='x')return(cursor.x);
	if(c=='y')return(cursor.y);
	return(-1);
}

void pantalla::mover(int x,int y)
{

	COORD coord;
	cursor.x=x;coord.X=x;
	cursor.y=y;coord.Y=y;
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE),coord);
}

void pantalla::cls(bool a)
{
	int i,j;
	mover(0,0);
	for(i=0;i<25;i++)for(j=0;j<80;j++)if(a){printf("%c",texto[i][j].caracter);}else printf("%c",a);
	mover(0,0);
}

void pantalla::operator()(char c)
{
	int i;
	for(i=79;i>cursor.x;i--)texto[cursor.y][i].caracter=texto[cursor.y][i-1].caracter;
	texto[cursor.y][cursor.x].caracter=c;
	for(i=cursor.x;i<80;i++)printf("%c",texto[cursor.y][i].caracter);
	desplazar(77);
}

void pantalla::borrar()
{
	int i;
	for(i=cursor.x;i<79;i++)if(i>=0)texto[cursor.y][i-1].caracter=texto[cursor.y][i].caracter;
	texto[cursor.y][79].caracter=0;
	desplazar(75);
	for(i=cursor.x;i<80;i++)printf("%c",texto[cursor.y][i].caracter);
	desplazar(0);
}

void pantalla::desplazar(char d)
{
	if(d==0)mover(cursor.x,cursor.y);
	if(d==72)if(cursor.y>0)mover(cursor.x,cursor.y-1);
	if(d==75){if(cursor.x>0)mover(cursor.x-1,cursor.y);else if(cursor.y>0)mover(79,cursor.y-1);};
	if(d==77){if(cursor.x<79)mover(cursor.x+1,cursor.y);else if(cursor.y<24)mover(0,cursor.y+1);};
	if(d==80)if(cursor.y<24)mover(cursor.x,cursor.y+1);
}