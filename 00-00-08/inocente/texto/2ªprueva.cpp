// copia.c: Copia de ficheros
// Uso: copia <fichero_origen> <fichero_destino>

#include <stdio.h>

int main(int argc, char **argv) {
    FILE *fe, *fs;
	int uno=3;
	char **dos,tres[]="ejemplo1.c",cuatro[]="ejemplo2.c";
	dos=(char**)new(int[3]);
	dos[1]=tres;
	dos[2]=cuatro;
    unsigned char buffer[2048]; // Buffer de 2 Kbytes
    int bytesLeidos;

    if(uno != 3) {
       printf("Usar: copia <fichero_origen> <fichero_destino>\n");
       return 1;
    }

    // Abrir el fichero de entrada en lectura y binario
    fe = fopen(dos[1], "rb"); 
    if(!fe) {
       printf("El fichero %s no existe o no puede ser abierto.\n", dos[1]);
       return 1;
    }
    // Crear o sobreescribir el fichero de salida en binario
    fs = fopen(dos[2], "wb"); 
    if(!fs) {
       printf("El fichero %s no puede ser creado.\n", dos[2]);
       fclose(fe);
       return 1;
    }
    // Bucle de copia:
    while((bytesLeidos = fread(buffer, 1, 2048, fe)))
       fwrite(buffer, 1, bytesLeidos, fs);
    // Cerrar ficheros:
    fclose(fe);
    fclose(fs);
    return 0;
}
