#ifndef BIOS
#define BIOS
#include <conio.h>
#include <stdio.h>
#include <windows.h>
void mover(int x,int y);
char in(int x,int y);
int out(char c,int x,int y);
char in(int x,int y)
{
	mover(x,y);
	return(getch());
}
int out(char c,int x,int y)
{
	mover(x,y);
	return(printf("%c",c));
}
void mover(int x,int y)
{

	COORD coord;
	coord.X=x;
	coord.Y=y;
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE),coord);
}
#endif