#include <stdio.h>
#include <process.h>
#include <string.h>

char nameo[]="mia/orden.txt";
char named[]="orden.txt";
char version[]="1.2";

class palabra{
private:
	friend palabra *lector(FILE *archivo);
	char *cad;
	palabra *sig;
public:
	palabra(char *buf);
	~palabra();
	char *operator[](int i);
};

palabra::palabra(char *buf){
	int i=0;
	while(buf[i++]!='\0');
	cad=new char[i];
	sprintf(cad,buf);
	sig=NULL;
}

palabra::~palabra(){
	if(cad)delete cad;
	if(sig)delete sig;
}

char *palabra::operator [](int i){
	if(i>0)return (*sig)[--i];
	else return cad;
}

palabra *lector(FILE *archivo){
	palabra *raiz,*aux;
	int salida0=1,salida1=1,i;
	char buf[100];
	while(salida0){
		for(i=0;((buf[i]=fgetc(archivo))!=' ')&&(buf[i]!=-1);i++);
		if(buf[i]==-1)salida0=0;
		buf[i]='\0';
		if(salida1){
			raiz=new palabra(buf);
			aux=raiz;
			salida1=0;
		}
		else{
			aux->sig=new palabra(buf);
			aux=aux->sig;
		}
	}
	fclose(archivo);
	return raiz;
}

void main(){

	int salida0,salida1;
	FILE *archivo;
	palabra *ordenes;

	salida0=1;
	while(salida0){

		salida1=1;
		if((archivo=fopen(named,"r"))){fclose(archivo);remove(named);}
		do{

			_spawnl( _P_WAIT,"copia_de_archivos.exe","copia_de_archivos.exe",nameo,named,NULL);
			if((archivo=fopen(named,"r"))){
				salida1=0;
			}

		}while(salida1);

		ordenes=lector(archivo);
		remove(named);

		if(!strcmp((*ordenes)[0],"copiar")){
			_spawnl( _P_WAIT,"copia_de_archivos.exe","copia_de_archivos.exe",(*ordenes)[1],(*ordenes)[2],NULL);
		}
		if(!strcmp((*ordenes)[0],"abrir")){
			_spawnl( _P_NOWAIT,(*ordenes)[1],(*ordenes)[1],NULL);
			printf("\nya he vuelto");//debug
		}
		if(!strcmp((*ordenes)[0],"actualizar")){
			execl("paso.exe","paso.exe",NULL);
			printf("\nno");//debug
		}
		if(!strcmp((*ordenes)[0],"version")){
			_spawnl( _P_NOWAIT,"retorno.exe","retorno.exe",version,NULL);

		}
		if(!strcmp((*ordenes)[0],"cerrar")){
			salida0=0;
		}
	//	if(!strcmp((*ordenes)[0],"")){
	//	}

		delete ordenes;

	}

}