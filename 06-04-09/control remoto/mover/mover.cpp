#include <stdio.h>

int main(int argc,char *argv[]){

	FILE *org,*dest;
	char c,salida=0;

	if((org=fopen(argv[1],"r"))){
		dest=fopen(argv[2],"w");
		do{
			if(salida){
				fputc(c,dest);
			}
			c=fgetc(org);
			if(feof(org))salida=0;
			else salida=1;
		}while(salida);
		fclose(org);
		fclose(dest);
		remove(argv[1]);
	}

	return 0;
}