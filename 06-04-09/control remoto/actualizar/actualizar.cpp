#include <stdio.h>
#include <process.h>
#include <string.h>

char path[]="mia/";
char tmp[]="tmp/";
char buf[100],BUF[100];

int main(int argc,char *argv[]){

	sprintf(buf,"%s.exe",argv[1]);
	sprintf(BUF,"%s%s.exe",tmp,argv[1]);
	_spawnl(_P_WAIT,"mover.exe","mover.exe",buf,BUF,NULL);

	sprintf(BUF,"%s%s.exe",path,argv[1]);
	_spawnl(_P_WAIT,"mover.exe","mover.exe",BUF,buf,NULL);

	_spawnl(_P_WAIT,"info.exe","info.exe",argv[1],argv[2],NULL);

	if(!strcmp(argv[1],"nucleo")){
		execl("nucleo.exe","nucleo.exe",NULL);
	}

	return 0;
}